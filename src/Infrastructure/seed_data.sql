delete from Players;
delete from Teams;
 
set identity_insert Teams on
insert into Teams
  (Id, name)
select *
from openjson('[
  {
    "Id": 1,
    "Name": "New Jersey Devils"
  },
  {
    "Id": 2,
    "Name": "New York Islanders"
  },
  {
    "Id": 3,
    "Name": "New York Rangers"
  },
  {
    "Id": 4,
    "Name": "Philadelphia Flyers"
  },
  {
    "Id": 5,
    "Name": "Pittsburgh Penguins"
  },
  {
    "Id": 6,
    "Name": "Boston Bruins"
  },
  {
    "Id": 7,
    "Name": "Buffalo Sabres"
  },
  {
    "Id": 8,
    "Name": "Montr\u00E9al Canadiens"
  },
  {
    "Id": 9,
    "Name": "Ottawa Senators"
  },
  {
    "Id": 10,
    "Name": "Toronto Maple Leafs"
  },
  {
    "Id": 12,
    "Name": "Carolina Hurricanes"
  },
  {
    "Id": 13,
    "Name": "Florida Panthers"
  },
  {
    "Id": 14,
    "Name": "Tampa Bay Lightning"
  },
  {
    "Id": 15,
    "Name": "Washington Capitals"
  },
  {
    "Id": 16,
    "Name": "Chicago Blackhawks"
  },
  {
    "Id": 17,
    "Name": "Detroit Red Wings"
  },
  {
    "Id": 18,
    "Name": "Nashville Predators"
  },
  {
    "Id": 19,
    "Name": "St. Louis Blues"
  },
  {
    "Id": 20,
    "Name": "Calgary Flames"
  },
  {
    "Id": 21,
    "Name": "Colorado Avalanche"
  },
  {
    "Id": 22,
    "Name": "Edmonton Oilers"
  },
  {
    "Id": 23,
    "Name": "Vancouver Canucks"
  },
  {
    "Id": 24,
    "Name": "Anaheim Ducks"
  },
  {
    "Id": 25,
    "Name": "Dallas Stars"
  },
  {
    "Id": 26,
    "Name": "Los Angeles Kings"
  },
  {
    "Id": 28,
    "Name": "San Jose Sharks"
  },
  {
    "Id": 29,
    "Name": "Columbus Blue Jackets"
  },
  {
    "Id": 30,
    "Name": "Minnesota Wild"
  },
  {
    "Id": 52,
    "Name": "Winnipeg Jets"
  },
  {
    "Id": 53,
    "Name": "Arizona Coyotes"
  },
  {
    "Id": 54,
    "Name": "Vegas Golden Knights"
  }
]') with (Id int, Name nvarchar(max))
set identity_insert Teams off
 
set identity_insert Players on
insert
into Players
(name, JerseyNumber, TeamId, Id)
select *
from openjson('[
  {
    "Name": "Josh Jacobs",
    "JerseyNumber": 40,
    "TeamId": 1,
    "Id": 1
  },
  {
    "Name": "P.K. Subban",
    "JerseyNumber": 76,
    "TeamId": 1,
    "Id": 2
  },
  {
    "Name": "Scott Wedgewood",
    "JerseyNumber": 41,
    "TeamId": 1,
    "Id": 3
  },
  {
    "Name": "Ryan Murray",
    "JerseyNumber": 22,
    "TeamId": 1,
    "Id": 4
  },
  {
    "Name": "Damon Severson",
    "JerseyNumber": 28,
    "TeamId": 1,
    "Id": 5
  },
  {
    "Name": "Aaron Dell",
    "JerseyNumber": 47,
    "TeamId": 1,
    "Id": 6
  },
  {
    "Name": "Andreas Johnsson",
    "JerseyNumber": 11,
    "TeamId": 1,
    "Id": 7
  },
  {
    "Name": "Will Butcher",
    "JerseyNumber": 8,
    "TeamId": 1,
    "Id": 8
  },
  {
    "Name": "Miles Wood",
    "JerseyNumber": 44,
    "TeamId": 1,
    "Id": 9
  },
  {
    "Name": "Jonas Siegenthaler",
    "JerseyNumber": 71,
    "TeamId": 1,
    "Id": 10
  },
  {
    "Name": "Pavel Zacha",
    "JerseyNumber": 37,
    "TeamId": 1,
    "Id": 11
  },
  {
    "Name": "Mackenzie Blackwood",
    "JerseyNumber": 29,
    "TeamId": 1,
    "Id": 12
  },
  {
    "Name": "Nicholas Merkley",
    "JerseyNumber": 39,
    "TeamId": 1,
    "Id": 13
  },
  {
    "Name": "Jesper Bratt",
    "JerseyNumber": 63,
    "TeamId": 1,
    "Id": 14
  },
  {
    "Name": "Nathan Bastian",
    "JerseyNumber": 14,
    "TeamId": 1,
    "Id": 15
  },
  {
    "Name": "Michael McLeod",
    "JerseyNumber": 20,
    "TeamId": 1,
    "Id": 16
  },
  {
    "Name": "Janne Kuokkanen",
    "JerseyNumber": 59,
    "TeamId": 1,
    "Id": 17
  },
  {
    "Name": "Mikhail Maltsev",
    "JerseyNumber": 23,
    "TeamId": 1,
    "Id": 18
  },
  {
    "Name": "Nico Hischier",
    "JerseyNumber": 13,
    "TeamId": 1,
    "Id": 19
  },
  {
    "Name": "Jesper Boqvist",
    "JerseyNumber": 90,
    "TeamId": 1,
    "Id": 20
  },
  {
    "Name": "Ty Smith",
    "JerseyNumber": 24,
    "TeamId": 1,
    "Id": 21
  },
  {
    "Name": "Yegor Sharangovich",
    "JerseyNumber": 17,
    "TeamId": 1,
    "Id": 22
  },
  {
    "Name": "Jack Hughes",
    "JerseyNumber": 86,
    "TeamId": 1,
    "Id": 23
  },
  {
    "Name": "Anders Lee",
    "JerseyNumber": 27,
    "TeamId": 2,
    "Id": 24
  },
  {
    "Name": "Johnny Boychuk",
    "JerseyNumber": 55,
    "TeamId": 2,
    "Id": 25
  },
  {
    "Name": "Andrew Ladd",
    "JerseyNumber": 16,
    "TeamId": 2,
    "Id": 26
  },
  {
    "Name": "Thomas Hickey",
    "JerseyNumber": 34,
    "TeamId": 2,
    "Id": 27
  },
  {
    "Name": "Austin Czarnik",
    "JerseyNumber": 11,
    "TeamId": 2,
    "Id": 28
  },
  {
    "Name": "Kieffer Bellows",
    "JerseyNumber": 20,
    "TeamId": 2,
    "Id": 29
  },
  {
    "Name": "Braydon Coburn",
    "JerseyNumber": 45,
    "TeamId": 2,
    "Id": 30
  },
  {
    "Name": "Travis Zajac",
    "JerseyNumber": 14,
    "TeamId": 2,
    "Id": 31
  },
  {
    "Name": "Cory Schneider",
    "JerseyNumber": 35,
    "TeamId": 2,
    "Id": 32
  },
  {
    "Name": "Andy Greene",
    "JerseyNumber": 4,
    "TeamId": 2,
    "Id": 33
  },
  {
    "Name": "Leo Komarov",
    "JerseyNumber": 47,
    "TeamId": 2,
    "Id": 34
  },
  {
    "Name": "Cal Clutterbuck",
    "JerseyNumber": 15,
    "TeamId": 2,
    "Id": 35
  },
  {
    "Name": "Semyon Varlamov",
    "JerseyNumber": 40,
    "TeamId": 2,
    "Id": 36
  },
  {
    "Name": "Josh Bailey",
    "JerseyNumber": 12,
    "TeamId": 2,
    "Id": 37
  },
  {
    "Name": "Jordan Eberle",
    "JerseyNumber": 7,
    "TeamId": 2,
    "Id": 38
  },
  {
    "Name": "Matt Martin",
    "JerseyNumber": 17,
    "TeamId": 2,
    "Id": 39
  },
  {
    "Name": "Kyle Palmieri",
    "JerseyNumber": 21,
    "TeamId": 2,
    "Id": 40
  },
  {
    "Name": "Nick Leddy",
    "JerseyNumber": 2,
    "TeamId": 2,
    "Id": 41
  },
  {
    "Name": "Casey Cizikas",
    "JerseyNumber": 53,
    "TeamId": 2,
    "Id": 42
  },
  {
    "Name": "Brock Nelson",
    "JerseyNumber": 29,
    "TeamId": 2,
    "Id": 43
  },
  {
    "Name": "Jean-Gabriel Pageau",
    "JerseyNumber": 44,
    "TeamId": 2,
    "Id": 44
  },
  {
    "Name": "Scott Mayfield",
    "JerseyNumber": 24,
    "TeamId": 2,
    "Id": 45
  },
  {
    "Name": "Adam Pelech",
    "JerseyNumber": 3,
    "TeamId": 2,
    "Id": 46
  },
  {
    "Name": "Ryan Pulock",
    "JerseyNumber": 6,
    "TeamId": 2,
    "Id": 47
  },
  {
    "Name": "Ross Johnston",
    "JerseyNumber": 32,
    "TeamId": 2,
    "Id": 48
  },
  {
    "Name": "Michael Dal Colle",
    "JerseyNumber": 28,
    "TeamId": 2,
    "Id": 49
  },
  {
    "Name": "Ilya Sorokin",
    "JerseyNumber": 30,
    "TeamId": 2,
    "Id": 50
  },
  {
    "Name": "Mathew Barzal",
    "JerseyNumber": 13,
    "TeamId": 2,
    "Id": 51
  },
  {
    "Name": "Anthony Beauvillier",
    "JerseyNumber": 18,
    "TeamId": 2,
    "Id": 52
  },
  {
    "Name": "Sebastian Aho",
    "JerseyNumber": 25,
    "TeamId": 2,
    "Id": 53
  },
  {
    "Name": "Oliver Wahlstrom",
    "JerseyNumber": 26,
    "TeamId": 2,
    "Id": 54
  },
  {
    "Name": "Noah Dobson",
    "JerseyNumber": 8,
    "TeamId": 2,
    "Id": 55
  },
  {
    "Name": "Keith Kinkaid",
    "JerseyNumber": 71,
    "TeamId": 3,
    "Id": 56
  },
  {
    "Name": "Brendan Smith",
    "JerseyNumber": 42,
    "TeamId": 3,
    "Id": 57
  },
  {
    "Name": "Chris Kreider",
    "JerseyNumber": 20,
    "TeamId": 3,
    "Id": 58
  },
  {
    "Name": "Anthony Bitetto",
    "JerseyNumber": 22,
    "TeamId": 3,
    "Id": 59
  },
  {
    "Name": "Colin Blackwell",
    "JerseyNumber": 43,
    "TeamId": 3,
    "Id": 60
  },
  {
    "Name": "Ryan Strome",
    "JerseyNumber": 16,
    "TeamId": 3,
    "Id": 61
  },
  {
    "Name": "Mika Zibanejad",
    "JerseyNumber": 93,
    "TeamId": 3,
    "Id": 62
  },
  {
    "Name": "Phillip Di Giuseppe",
    "JerseyNumber": 33,
    "TeamId": 3,
    "Id": 63
  },
  {
    "Name": "Jacob Trouba",
    "JerseyNumber": 8,
    "TeamId": 3,
    "Id": 64
  },
  {
    "Name": "Pavel Buchnevich",
    "JerseyNumber": 89,
    "TeamId": 3,
    "Id": 65
  },
  {
    "Name": "Igor Shesterkin",
    "JerseyNumber": 31,
    "TeamId": 3,
    "Id": 66
  },
  {
    "Name": "Artemi Panarin",
    "JerseyNumber": 10,
    "TeamId": 3,
    "Id": 67
  },
  {
    "Name": "Kevin Rooney",
    "JerseyNumber": 17,
    "TeamId": 3,
    "Id": 68
  },
  {
    "Name": "Adam Fox",
    "JerseyNumber": 23,
    "TeamId": 3,
    "Id": 69
  },
  {
    "Name": "Ryan Lindgren",
    "JerseyNumber": 55,
    "TeamId": 3,
    "Id": 70
  },
  {
    "Name": "Julien Gauthier",
    "JerseyNumber": 12,
    "TeamId": 3,
    "Id": 71
  },
  {
    "Name": "Libor Hajek",
    "JerseyNumber": 25,
    "TeamId": 3,
    "Id": 72
  },
  {
    "Name": "Brett Howden",
    "JerseyNumber": 21,
    "TeamId": 3,
    "Id": 73
  },
  {
    "Name": "Filip Chytil",
    "JerseyNumber": 72,
    "TeamId": 3,
    "Id": 74
  },
  {
    "Name": "Alexandar Georgiev",
    "JerseyNumber": 40,
    "TeamId": 3,
    "Id": 75
  },
  {
    "Name": "K\u0027Andre Miller",
    "JerseyNumber": 79,
    "TeamId": 3,
    "Id": 76
  },
  {
    "Name": "Vitali Kravtsov",
    "JerseyNumber": 74,
    "TeamId": 3,
    "Id": 77
  },
  {
    "Name": "Kaapo Kakko",
    "JerseyNumber": 24,
    "TeamId": 3,
    "Id": 78
  },
  {
    "Name": "Zac Jones",
    "JerseyNumber": 6,
    "TeamId": 3,
    "Id": 79
  },
  {
    "Name": "Alexis Lafreni\u00E8re",
    "JerseyNumber": 13,
    "TeamId": 3,
    "Id": 80
  },
  {
    "Name": "Morgan Frost",
    "JerseyNumber": 48,
    "TeamId": 4,
    "Id": 81
  },
  {
    "Name": "Brian Elliott",
    "JerseyNumber": 37,
    "TeamId": 4,
    "Id": 82
  },
  {
    "Name": "Claude Giroux",
    "JerseyNumber": 28,
    "TeamId": 4,
    "Id": 83
  },
  {
    "Name": "Justin Braun",
    "JerseyNumber": 61,
    "TeamId": 4,
    "Id": 84
  },
  {
    "Name": "James van Riemsdyk",
    "JerseyNumber": 25,
    "TeamId": 4,
    "Id": 85
  },
  {
    "Name": "Jakub Voracek",
    "JerseyNumber": 93,
    "TeamId": 4,
    "Id": 86
  },
  {
    "Name": "Kevin Hayes",
    "JerseyNumber": 13,
    "TeamId": 4,
    "Id": 87
  },
  {
    "Name": "Sean Couturier",
    "JerseyNumber": 14,
    "TeamId": 4,
    "Id": 88
  },
  {
    "Name": "Scott Laughton",
    "JerseyNumber": 21,
    "TeamId": 4,
    "Id": 89
  },
  {
    "Name": "Shayne Gostisbehere",
    "JerseyNumber": 53,
    "TeamId": 4,
    "Id": 90
  },
  {
    "Name": "Robert Hagg",
    "JerseyNumber": 8,
    "TeamId": 4,
    "Id": 91
  },
  {
    "Name": "Samuel Morin",
    "JerseyNumber": 55,
    "TeamId": 4,
    "Id": 92
  },
  {
    "Name": "Travis Sanheim",
    "JerseyNumber": 6,
    "TeamId": 4,
    "Id": 93
  },
  {
    "Name": "Nicolas Aube-Kubel",
    "JerseyNumber": 62,
    "TeamId": 4,
    "Id": 94
  },
  {
    "Name": "Oskar Lindblom",
    "JerseyNumber": 23,
    "TeamId": 4,
    "Id": 95
  },
  {
    "Name": "Travis Konecny",
    "JerseyNumber": 11,
    "TeamId": 4,
    "Id": 96
  },
  {
    "Name": "Ivan Provorov",
    "JerseyNumber": 9,
    "TeamId": 4,
    "Id": 97
  },
  {
    "Name": "Philippe Myers",
    "JerseyNumber": 5,
    "TeamId": 4,
    "Id": 98
  },
  {
    "Name": "Alex Lyon",
    "JerseyNumber": 34,
    "TeamId": 4,
    "Id": 99
  },
  {
    "Name": "Wade Allison",
    "JerseyNumber": 57,
    "TeamId": 4,
    "Id": 100
  },
  {
    "Name": "Carter Hart",
    "JerseyNumber": 79,
    "TeamId": 4,
    "Id": 101
  },
  {
    "Name": "Tanner Laczynski",
    "JerseyNumber": 58,
    "TeamId": 4,
    "Id": 102
  },
  {
    "Name": "Nolan Patrick",
    "JerseyNumber": 19,
    "TeamId": 4,
    "Id": 103
  },
  {
    "Name": "Joel Farabee",
    "JerseyNumber": 86,
    "TeamId": 4,
    "Id": 104
  },
  {
    "Name": "Evgeni Malkin",
    "JerseyNumber": 71,
    "TeamId": 5,
    "Id": 105
  },
  {
    "Name": "Kasperi Kapanen",
    "JerseyNumber": 42,
    "TeamId": 5,
    "Id": 106
  },
  {
    "Name": "Mark Friedman",
    "JerseyNumber": 52,
    "TeamId": 5,
    "Id": 107
  },
  {
    "Name": "Brandon Tanev",
    "JerseyNumber": 13,
    "TeamId": 5,
    "Id": 108
  },
  {
    "Name": "Yannick Weber",
    "JerseyNumber": 3,
    "TeamId": 5,
    "Id": 109
  },
  {
    "Name": "Anthony Angello",
    "JerseyNumber": 57,
    "TeamId": 5,
    "Id": 110
  },
  {
    "Name": "Jordy Bellerive",
    "JerseyNumber": 64,
    "TeamId": 5,
    "Id": 111
  },
  {
    "Name": "Jonathan Gruden",
    "JerseyNumber": 45,
    "TeamId": 5,
    "Id": 112
  },
  {
    "Name": "Juuso Riikola",
    "JerseyNumber": 50,
    "TeamId": 5,
    "Id": 113
  },
  {
    "Name": "Emil Larmi",
    "JerseyNumber": 60,
    "TeamId": 5,
    "Id": 114
  },
  {
    "Name": "Drew O\u0027Connor",
    "JerseyNumber": 10,
    "TeamId": 5,
    "Id": 115
  },
  {
    "Name": "Jeff Carter",
    "JerseyNumber": 77,
    "TeamId": 5,
    "Id": 116
  },
  {
    "Name": "Sidney Crosby",
    "JerseyNumber": 87,
    "TeamId": 5,
    "Id": 117
  },
  {
    "Name": "Kris Letang",
    "JerseyNumber": 58,
    "TeamId": 5,
    "Id": 118
  },
  {
    "Name": "Colton Sceviour",
    "JerseyNumber": 7,
    "TeamId": 5,
    "Id": 119
  },
  {
    "Name": "Brian Dumoulin",
    "JerseyNumber": 8,
    "TeamId": 5,
    "Id": 120
  },
  {
    "Name": "Jason Zucker",
    "JerseyNumber": 16,
    "TeamId": 5,
    "Id": 121
  },
  {
    "Name": "Bryan Rust",
    "JerseyNumber": 17,
    "TeamId": 5,
    "Id": 122
  },
  {
    "Name": "Mark Jankowski",
    "JerseyNumber": 14,
    "TeamId": 5,
    "Id": 123
  },
  {
    "Name": "Mike Matheson",
    "JerseyNumber": 5,
    "TeamId": 5,
    "Id": 124
  },
  {
    "Name": "Cody Ceci",
    "JerseyNumber": 4,
    "TeamId": 5,
    "Id": 125
  },
  {
    "Name": "Teddy Blueger",
    "JerseyNumber": 53,
    "TeamId": 5,
    "Id": 126
  },
  {
    "Name": "Chad Ruhwedel",
    "JerseyNumber": 2,
    "TeamId": 5,
    "Id": 127
  },
  {
    "Name": "Jake Guentzel",
    "JerseyNumber": 59,
    "TeamId": 5,
    "Id": 128
  },
  {
    "Name": "Tristan Jarry",
    "JerseyNumber": 35,
    "TeamId": 5,
    "Id": 129
  },
  {
    "Name": "Frederick Gaudreau",
    "JerseyNumber": 11,
    "TeamId": 5,
    "Id": 130
  },
  {
    "Name": "Jared McCann",
    "JerseyNumber": 19,
    "TeamId": 5,
    "Id": 131
  },
  {
    "Name": "Marcus Pettersson",
    "JerseyNumber": 28,
    "TeamId": 5,
    "Id": 132
  },
  {
    "Name": "Sam Lafferty",
    "JerseyNumber": 18,
    "TeamId": 5,
    "Id": 133
  },
  {
    "Name": "John Marino",
    "JerseyNumber": 6,
    "TeamId": 5,
    "Id": 134
  },
  {
    "Name": "Evan Rodrigues",
    "JerseyNumber": 9,
    "TeamId": 5,
    "Id": 135
  },
  {
    "Name": "Casey DeSmith",
    "JerseyNumber": 1,
    "TeamId": 5,
    "Id": 136
  },
  {
    "Name": "Zach Aston-Reese",
    "JerseyNumber": 12,
    "TeamId": 5,
    "Id": 137
  },
  {
    "Name": "Radim Zohorna",
    "JerseyNumber": 67,
    "TeamId": 5,
    "Id": 138
  },
  {
    "Name": "John Moore",
    "JerseyNumber": 27,
    "TeamId": 6,
    "Id": 139
  },
  {
    "Name": "Ondrej Kase",
    "JerseyNumber": 28,
    "TeamId": 6,
    "Id": 140
  },
  {
    "Name": "Brandon Carlo",
    "JerseyNumber": 25,
    "TeamId": 6,
    "Id": 141
  },
  {
    "Name": "Patrice Bergeron",
    "JerseyNumber": 37,
    "TeamId": 6,
    "Id": 142
  },
  {
    "Name": "David Krejci",
    "JerseyNumber": 46,
    "TeamId": 6,
    "Id": 143
  },
  {
    "Name": "Tuukka Rask",
    "JerseyNumber": 40,
    "TeamId": 6,
    "Id": 144
  },
  {
    "Name": "Brad Marchand",
    "JerseyNumber": 63,
    "TeamId": 6,
    "Id": 145
  },
  {
    "Name": "Craig Smith",
    "JerseyNumber": 12,
    "TeamId": 6,
    "Id": 146
  },
  {
    "Name": "Charlie Coyle",
    "JerseyNumber": 13,
    "TeamId": 6,
    "Id": 147
  },
  {
    "Name": "Chris Wagner",
    "JerseyNumber": 14,
    "TeamId": 6,
    "Id": 148
  },
  {
    "Name": "Taylor Hall",
    "JerseyNumber": 71,
    "TeamId": 6,
    "Id": 149
  },
  {
    "Name": "Jarred Tinordi",
    "JerseyNumber": 84,
    "TeamId": 6,
    "Id": 150
  },
  {
    "Name": "Kevan Miller",
    "JerseyNumber": 86,
    "TeamId": 6,
    "Id": 151
  },
  {
    "Name": "Sean Kuraly",
    "JerseyNumber": 52,
    "TeamId": 6,
    "Id": 152
  },
  {
    "Name": "Mike Reilly",
    "JerseyNumber": 6,
    "TeamId": 6,
    "Id": 153
  },
  {
    "Name": "Matt Grzelcyk",
    "JerseyNumber": 48,
    "TeamId": 6,
    "Id": 154
  },
  {
    "Name": "Anton Blidh",
    "JerseyNumber": 81,
    "TeamId": 6,
    "Id": 155
  },
  {
    "Name": "Connor Clifton",
    "JerseyNumber": 75,
    "TeamId": 6,
    "Id": 156
  },
  {
    "Name": "Curtis Lazar",
    "JerseyNumber": 20,
    "TeamId": 6,
    "Id": 157
  },
  {
    "Name": "Nick Ritchie",
    "JerseyNumber": 21,
    "TeamId": 6,
    "Id": 158
  },
  {
    "Name": "David Pastrnak",
    "JerseyNumber": 88,
    "TeamId": 6,
    "Id": 159
  },
  {
    "Name": "Jakub Zboril",
    "JerseyNumber": 67,
    "TeamId": 6,
    "Id": 160
  },
  {
    "Name": "Dan Vladar",
    "JerseyNumber": 80,
    "TeamId": 6,
    "Id": 161
  },
  {
    "Name": "Jeremy Lauzon",
    "JerseyNumber": 55,
    "TeamId": 6,
    "Id": 162
  },
  {
    "Name": "Jake DeBrusk",
    "JerseyNumber": 74,
    "TeamId": 6,
    "Id": 163
  },
  {
    "Name": "Charlie McAvoy",
    "JerseyNumber": 73,
    "TeamId": 6,
    "Id": 164
  },
  {
    "Name": "Jeremy Swayman",
    "JerseyNumber": 1,
    "TeamId": 6,
    "Id": 165
  },
  {
    "Name": "Karson Kuhlman",
    "JerseyNumber": 83,
    "TeamId": 6,
    "Id": 166
  },
  {
    "Name": "Zemgus Girgensons",
    "JerseyNumber": 28,
    "TeamId": 7,
    "Id": 167
  },
  {
    "Name": "Jake McCabe",
    "JerseyNumber": 19,
    "TeamId": 7,
    "Id": 168
  },
  {
    "Name": "Jack Eichel",
    "JerseyNumber": 9,
    "TeamId": 7,
    "Id": 169
  },
  {
    "Name": "William Borgen",
    "JerseyNumber": 3,
    "TeamId": 7,
    "Id": 170
  },
  {
    "Name": "Brandon Davidson",
    "JerseyNumber": 88,
    "TeamId": 7,
    "Id": 171
  },
  {
    "Name": "Michael Houser",
    "JerseyNumber": 32,
    "TeamId": 7,
    "Id": 172
  },
  {
    "Name": "Drake Caggiula",
    "JerseyNumber": 91,
    "TeamId": 7,
    "Id": 173
  },
  {
    "Name": "C.J. Smith",
    "JerseyNumber": 49,
    "TeamId": 7,
    "Id": 174
  },
  {
    "Name": "Kyle Okposo",
    "JerseyNumber": 21,
    "TeamId": 7,
    "Id": 175
  },
  {
    "Name": "Dustin Tokarski",
    "JerseyNumber": 31,
    "TeamId": 7,
    "Id": 176
  },
  {
    "Name": "Cody Eakin",
    "JerseyNumber": 20,
    "TeamId": 7,
    "Id": 177
  },
  {
    "Name": "Carter Hutton",
    "JerseyNumber": 40,
    "TeamId": 7,
    "Id": 178
  },
  {
    "Name": "Matt Irwin",
    "JerseyNumber": 44,
    "TeamId": 7,
    "Id": 179
  },
  {
    "Name": "Riley Sheahan",
    "JerseyNumber": 15,
    "TeamId": 7,
    "Id": 180
  },
  {
    "Name": "Jeff Skinner",
    "JerseyNumber": 53,
    "TeamId": 7,
    "Id": 181
  },
  {
    "Name": "Tobias Rieder",
    "JerseyNumber": 13,
    "TeamId": 7,
    "Id": 182
  },
  {
    "Name": "Colin Miller",
    "JerseyNumber": 33,
    "TeamId": 7,
    "Id": 183
  },
  {
    "Name": "Linus Ullmark",
    "JerseyNumber": 35,
    "TeamId": 7,
    "Id": 184
  },
  {
    "Name": "Rasmus Ristolainen",
    "JerseyNumber": 55,
    "TeamId": 7,
    "Id": 185
  },
  {
    "Name": "Sam Reinhart",
    "JerseyNumber": 23,
    "TeamId": 7,
    "Id": 186
  },
  {
    "Name": "Anders Bjork",
    "JerseyNumber": 96,
    "TeamId": 7,
    "Id": 187
  },
  {
    "Name": "Victor Olofsson",
    "JerseyNumber": 68,
    "TeamId": 7,
    "Id": 188
  },
  {
    "Name": "Rasmus Asplund",
    "JerseyNumber": 74,
    "TeamId": 7,
    "Id": 189
  },
  {
    "Name": "Tage Thompson",
    "JerseyNumber": 72,
    "TeamId": 7,
    "Id": 190
  },
  {
    "Name": "Casey Mittelstadt",
    "JerseyNumber": 37,
    "TeamId": 7,
    "Id": 191
  },
  {
    "Name": "Henri Jokiharju",
    "JerseyNumber": 10,
    "TeamId": 7,
    "Id": 192
  },
  {
    "Name": "Jacob Bryson",
    "JerseyNumber": 78,
    "TeamId": 7,
    "Id": 193
  },
  {
    "Name": "Rasmus Dahlin",
    "JerseyNumber": 26,
    "TeamId": 7,
    "Id": 194
  },
  {
    "Name": "Dylan Cozens",
    "JerseyNumber": 24,
    "TeamId": 7,
    "Id": 195
  },
  {
    "Name": "Arttu Ruotsalainen",
    "JerseyNumber": 25,
    "TeamId": 7,
    "Id": 196
  },
  {
    "Name": "Brendan Gallagher",
    "JerseyNumber": 11,
    "TeamId": 8,
    "Id": 197
  },
  {
    "Name": "Michael Frolik",
    "JerseyNumber": 67,
    "TeamId": 8,
    "Id": 198
  },
  {
    "Name": "Xavier Ouellet",
    "JerseyNumber": 61,
    "TeamId": 8,
    "Id": 199
  },
  {
    "Name": "Jake Evans",
    "JerseyNumber": 71,
    "TeamId": 8,
    "Id": 200
  },
  {
    "Name": "Michael McNiven",
    "JerseyNumber": 70,
    "TeamId": 8,
    "Id": 201
  },
  {
    "Name": "Cole Caufield",
    "JerseyNumber": 22,
    "TeamId": 8,
    "Id": 202
  },
  {
    "Name": "Eric Staal",
    "JerseyNumber": 21,
    "TeamId": 8,
    "Id": 203
  },
  {
    "Name": "Corey Perry",
    "JerseyNumber": 94,
    "TeamId": 8,
    "Id": 204
  },
  {
    "Name": "Shea Weber",
    "JerseyNumber": 6,
    "TeamId": 8,
    "Id": 205
  },
  {
    "Name": "Carey Price",
    "JerseyNumber": 31,
    "TeamId": 8,
    "Id": 206
  },
  {
    "Name": "Jeff Petry",
    "JerseyNumber": 26,
    "TeamId": 8,
    "Id": 207
  },
  {
    "Name": "Paul Byron",
    "JerseyNumber": 41,
    "TeamId": 8,
    "Id": 208
  },
  {
    "Name": "Jake Allen",
    "JerseyNumber": 34,
    "TeamId": 8,
    "Id": 209
  },
  {
    "Name": "Tomas Tatar",
    "JerseyNumber": 90,
    "TeamId": 8,
    "Id": 210
  },
  {
    "Name": "Ben Chiarot",
    "JerseyNumber": 8,
    "TeamId": 8,
    "Id": 211
  },
  {
    "Name": "Tyler Toffoli",
    "JerseyNumber": 73,
    "TeamId": 8,
    "Id": 212
  },
  {
    "Name": "Jon Merrill",
    "JerseyNumber": 28,
    "TeamId": 8,
    "Id": 213
  },
  {
    "Name": "Joel Edmundson",
    "JerseyNumber": 44,
    "TeamId": 8,
    "Id": 214
  },
  {
    "Name": "Joel Armia",
    "JerseyNumber": 40,
    "TeamId": 8,
    "Id": 215
  },
  {
    "Name": "Phillip Danault",
    "JerseyNumber": 24,
    "TeamId": 8,
    "Id": 216
  },
  {
    "Name": "Brett Kulak",
    "JerseyNumber": 77,
    "TeamId": 8,
    "Id": 217
  },
  {
    "Name": "Erik Gustafsson",
    "JerseyNumber": 32,
    "TeamId": 8,
    "Id": 218
  },
  {
    "Name": "Josh Anderson",
    "JerseyNumber": 17,
    "TeamId": 8,
    "Id": 219
  },
  {
    "Name": "Artturi Lehkonen",
    "JerseyNumber": 62,
    "TeamId": 8,
    "Id": 220
  },
  {
    "Name": "Jonathan Drouin",
    "JerseyNumber": 92,
    "TeamId": 8,
    "Id": 221
  },
  {
    "Name": "Nick Suzuki",
    "JerseyNumber": 14,
    "TeamId": 8,
    "Id": 222
  },
  {
    "Name": "Cayden Primeau",
    "JerseyNumber": 30,
    "TeamId": 8,
    "Id": 223
  },
  {
    "Name": "Jesperi Kotkaniemi",
    "JerseyNumber": 15,
    "TeamId": 8,
    "Id": 224
  },
  {
    "Name": "Alexander Romanov",
    "JerseyNumber": 27,
    "TeamId": 8,
    "Id": 225
  },
  {
    "Name": "Derek Stepan",
    "JerseyNumber": 21,
    "TeamId": 9,
    "Id": 226
  },
  {
    "Name": "Austin Watson",
    "JerseyNumber": 16,
    "TeamId": 9,
    "Id": 227
  },
  {
    "Name": "Joey Daccord",
    "JerseyNumber": 34,
    "TeamId": 9,
    "Id": 228
  },
  {
    "Name": "Micheal Haley",
    "JerseyNumber": 38,
    "TeamId": 9,
    "Id": 229
  },
  {
    "Name": "Artem Anisimov",
    "JerseyNumber": 51,
    "TeamId": 9,
    "Id": 230
  },
  {
    "Name": "Evgenii Dadonov",
    "JerseyNumber": 63,
    "TeamId": 9,
    "Id": 231
  },
  {
    "Name": "Ryan Dzingel",
    "JerseyNumber": 10,
    "TeamId": 9,
    "Id": 232
  },
  {
    "Name": "Anton Forsberg",
    "JerseyNumber": 31,
    "TeamId": 9,
    "Id": 233
  },
  {
    "Name": "Matt Murray",
    "JerseyNumber": 30,
    "TeamId": 9,
    "Id": 234
  },
  {
    "Name": "Chris Tierney",
    "JerseyNumber": 71,
    "TeamId": 9,
    "Id": 235
  },
  {
    "Name": "Connor Brown",
    "JerseyNumber": 28,
    "TeamId": 9,
    "Id": 236
  },
  {
    "Name": "Josh Brown",
    "JerseyNumber": 3,
    "TeamId": 9,
    "Id": 237
  },
  {
    "Name": "Marcus Hogberg",
    "JerseyNumber": 1,
    "TeamId": 9,
    "Id": 238
  },
  {
    "Name": "Nick Paul",
    "JerseyNumber": 13,
    "TeamId": 9,
    "Id": 239
  },
  {
    "Name": "Michael Amadio",
    "JerseyNumber": 29,
    "TeamId": 9,
    "Id": 240
  },
  {
    "Name": "Colin White",
    "JerseyNumber": 36,
    "TeamId": 9,
    "Id": 241
  },
  {
    "Name": "Thomas Chabot",
    "JerseyNumber": 72,
    "TeamId": 9,
    "Id": 242
  },
  {
    "Name": "Victor Mete",
    "JerseyNumber": 98,
    "TeamId": 9,
    "Id": 243
  },
  {
    "Name": "Nikita Zaitsev",
    "JerseyNumber": 22,
    "TeamId": 9,
    "Id": 244
  },
  {
    "Name": "Josh Norris",
    "JerseyNumber": 9,
    "TeamId": 9,
    "Id": 245
  },
  {
    "Name": "Erik Brannstrom",
    "JerseyNumber": 26,
    "TeamId": 9,
    "Id": 246
  },
  {
    "Name": "Drake Batherson",
    "JerseyNumber": 19,
    "TeamId": 9,
    "Id": 247
  },
  {
    "Name": "Brady Tkachuk",
    "JerseyNumber": 7,
    "TeamId": 9,
    "Id": 248
  },
  {
    "Name": "Jacob Bernard-Docker",
    "JerseyNumber": 48,
    "TeamId": 9,
    "Id": 249
  },
  {
    "Name": "Shane Pinto",
    "JerseyNumber": 57,
    "TeamId": 9,
    "Id": 250
  },
  {
    "Name": "Tim St\u00FCtzle",
    "JerseyNumber": 18,
    "TeamId": 9,
    "Id": 251
  },
  {
    "Name": "Artem Zub",
    "JerseyNumber": 2,
    "TeamId": 9,
    "Id": 252
  },
  {
    "Name": "Riley Nash",
    "JerseyNumber": 20,
    "TeamId": 10,
    "Id": 253
  },
  {
    "Name": "Frederik Andersen",
    "JerseyNumber": 31,
    "TeamId": 10,
    "Id": 254
  },
  {
    "Name": "Rasmus Sandin",
    "JerseyNumber": 38,
    "TeamId": 10,
    "Id": 255
  },
  {
    "Name": "Joe Thornton",
    "JerseyNumber": 97,
    "TeamId": 10,
    "Id": 256
  },
  {
    "Name": "Jason Spezza",
    "JerseyNumber": 19,
    "TeamId": 10,
    "Id": 257
  },
  {
    "Name": "Nick Foligno",
    "JerseyNumber": 71,
    "TeamId": 10,
    "Id": 258
  },
  {
    "Name": "Jake Muzzin",
    "JerseyNumber": 8,
    "TeamId": 10,
    "Id": 259
  },
  {
    "Name": "Wayne Simmonds",
    "JerseyNumber": 24,
    "TeamId": 10,
    "Id": 260
  },
  {
    "Name": "Zach Bogosian",
    "JerseyNumber": 22,
    "TeamId": 10,
    "Id": 261
  },
  {
    "Name": "TJ Brodie",
    "JerseyNumber": 78,
    "TeamId": 10,
    "Id": 262
  },
  {
    "Name": "John Tavares",
    "JerseyNumber": 91,
    "TeamId": 10,
    "Id": 263
  },
  {
    "Name": "Justin Holl",
    "JerseyNumber": 3,
    "TeamId": 10,
    "Id": 264
  },
  {
    "Name": "Zach Hyman",
    "JerseyNumber": 11,
    "TeamId": 10,
    "Id": 265
  },
  {
    "Name": "Jack Campbell",
    "JerseyNumber": 36,
    "TeamId": 10,
    "Id": 266
  },
  {
    "Name": "Alex Galchenyuk",
    "JerseyNumber": 12,
    "TeamId": 10,
    "Id": 267
  },
  {
    "Name": "Morgan Rielly",
    "JerseyNumber": 44,
    "TeamId": 10,
    "Id": 268
  },
  {
    "Name": "Ben Hutton",
    "JerseyNumber": 55,
    "TeamId": 10,
    "Id": 269
  },
  {
    "Name": "Alexander Kerfoot",
    "JerseyNumber": 15,
    "TeamId": 10,
    "Id": 270
  },
  {
    "Name": "Scott Sabourin",
    "JerseyNumber": 49,
    "TeamId": 10,
    "Id": 271
  },
  {
    "Name": "William Nylander",
    "JerseyNumber": 88,
    "TeamId": 10,
    "Id": 272
  },
  {
    "Name": "Pierre Engvall",
    "JerseyNumber": 47,
    "TeamId": 10,
    "Id": 273
  },
  {
    "Name": "Travis Dermott",
    "JerseyNumber": 23,
    "TeamId": 10,
    "Id": 274
  },
  {
    "Name": "Mitchell Marner",
    "JerseyNumber": 16,
    "TeamId": 10,
    "Id": 275
  },
  {
    "Name": "Auston Matthews",
    "JerseyNumber": 34,
    "TeamId": 10,
    "Id": 276
  },
  {
    "Name": "David Rittich",
    "JerseyNumber": 33,
    "TeamId": 10,
    "Id": 277
  },
  {
    "Name": "Nicholas Robertson",
    "JerseyNumber": 89,
    "TeamId": 10,
    "Id": 278
  },
  {
    "Name": "Ilya Mikheyev",
    "JerseyNumber": 65,
    "TeamId": 10,
    "Id": 279
  },
  {
    "Name": "Teuvo Teravainen",
    "JerseyNumber": 86,
    "TeamId": 12,
    "Id": 280
  },
  {
    "Name": "James Reimer",
    "JerseyNumber": 47,
    "TeamId": 12,
    "Id": 281
  },
  {
    "Name": "Jordan Staal",
    "JerseyNumber": 11,
    "TeamId": 12,
    "Id": 282
  },
  {
    "Name": "Jake Gardiner",
    "JerseyNumber": 51,
    "TeamId": 12,
    "Id": 283
  },
  {
    "Name": "Nino Niederreiter",
    "JerseyNumber": 21,
    "TeamId": 12,
    "Id": 284
  },
  {
    "Name": "Jani Hakanpaa",
    "JerseyNumber": 58,
    "TeamId": 12,
    "Id": 285
  },
  {
    "Name": "Petr Mrazek",
    "JerseyNumber": 34,
    "TeamId": 12,
    "Id": 286
  },
  {
    "Name": "Jesper Fast",
    "JerseyNumber": 71,
    "TeamId": 12,
    "Id": 287
  },
  {
    "Name": "Vincent Trocheck",
    "JerseyNumber": 16,
    "TeamId": 12,
    "Id": 288
  },
  {
    "Name": "Dougie Hamilton",
    "JerseyNumber": 19,
    "TeamId": 12,
    "Id": 289
  },
  {
    "Name": "Brady Skjei",
    "JerseyNumber": 76,
    "TeamId": 12,
    "Id": 290
  },
  {
    "Name": "Jordan Martinook",
    "JerseyNumber": 48,
    "TeamId": 12,
    "Id": 291
  },
  {
    "Name": "Brock McGinn",
    "JerseyNumber": 23,
    "TeamId": 12,
    "Id": 292
  },
  {
    "Name": "Jaccob Slavin",
    "JerseyNumber": 74,
    "TeamId": 12,
    "Id": 293
  },
  {
    "Name": "Cedric Paquette",
    "JerseyNumber": 18,
    "TeamId": 12,
    "Id": 294
  },
  {
    "Name": "Brett Pesce",
    "JerseyNumber": 22,
    "TeamId": 12,
    "Id": 295
  },
  {
    "Name": "Alex Nedeljkovic",
    "JerseyNumber": 39,
    "TeamId": 12,
    "Id": 296
  },
  {
    "Name": "Warren Foegele",
    "JerseyNumber": 13,
    "TeamId": 12,
    "Id": 297
  },
  {
    "Name": "Sebastian Aho",
    "JerseyNumber": 20,
    "TeamId": 12,
    "Id": 298
  },
  {
    "Name": "Steven Lorentz",
    "JerseyNumber": 78,
    "TeamId": 12,
    "Id": 299
  },
  {
    "Name": "Jake Bean",
    "JerseyNumber": 24,
    "TeamId": 12,
    "Id": 300
  },
  {
    "Name": "Martin Necas",
    "JerseyNumber": 88,
    "TeamId": 12,
    "Id": 301
  },
  {
    "Name": "Andrei Svechnikov",
    "JerseyNumber": 37,
    "TeamId": 12,
    "Id": 302
  },
  {
    "Name": "Aaron Ekblad",
    "JerseyNumber": 5,
    "TeamId": 13,
    "Id": 303
  },
  {
    "Name": "Kevin Connauton",
    "JerseyNumber": 44,
    "TeamId": 13,
    "Id": 304
  },
  {
    "Name": "Philippe Desrosiers",
    "JerseyNumber": 95,
    "TeamId": 13,
    "Id": 305
  },
  {
    "Name": "Lucas Carlsson",
    "JerseyNumber": 32,
    "TeamId": 13,
    "Id": 306
  },
  {
    "Name": "Aleksi Heponiemi",
    "JerseyNumber": 20,
    "TeamId": 13,
    "Id": 307
  },
  {
    "Name": "Brady Keeper",
    "JerseyNumber": 25,
    "TeamId": 13,
    "Id": 308
  },
  {
    "Name": "Keith Yandle",
    "JerseyNumber": 3,
    "TeamId": 13,
    "Id": 309
  },
  {
    "Name": "Anton Stralman",
    "JerseyNumber": 6,
    "TeamId": 13,
    "Id": 310
  },
  {
    "Name": "Patric Hornqvist",
    "JerseyNumber": 70,
    "TeamId": 13,
    "Id": 311
  },
  {
    "Name": "Radko Gudas",
    "JerseyNumber": 7,
    "TeamId": 13,
    "Id": 312
  },
  {
    "Name": "Sergei Bobrovsky",
    "JerseyNumber": 72,
    "TeamId": 13,
    "Id": 313
  },
  {
    "Name": "Jonathan Huberdeau",
    "JerseyNumber": 11,
    "TeamId": 13,
    "Id": 314
  },
  {
    "Name": "Chris Driedger",
    "JerseyNumber": 60,
    "TeamId": 13,
    "Id": 315
  },
  {
    "Name": "Nikita Gusev",
    "JerseyNumber": 97,
    "TeamId": 13,
    "Id": 316
  },
  {
    "Name": "MacKenzie Weegar",
    "JerseyNumber": 52,
    "TeamId": 13,
    "Id": 317
  },
  {
    "Name": "Anthony Duclair",
    "JerseyNumber": 91,
    "TeamId": 13,
    "Id": 318
  },
  {
    "Name": "Carter Verhaeghe",
    "JerseyNumber": 23,
    "TeamId": 13,
    "Id": 319
  },
  {
    "Name": "Aleksander Barkov",
    "JerseyNumber": 16,
    "TeamId": 13,
    "Id": 320
  },
  {
    "Name": "Alex Wennberg",
    "JerseyNumber": 21,
    "TeamId": 13,
    "Id": 321
  },
  {
    "Name": "Sam Bennett",
    "JerseyNumber": 9,
    "TeamId": 13,
    "Id": 322
  },
  {
    "Name": "Brandon Montour",
    "JerseyNumber": 62,
    "TeamId": 13,
    "Id": 323
  },
  {
    "Name": "Juho Lammikko",
    "JerseyNumber": 83,
    "TeamId": 13,
    "Id": 324
  },
  {
    "Name": "Lucas Wallmark",
    "JerseyNumber": 71,
    "TeamId": 13,
    "Id": 325
  },
  {
    "Name": "Gustav Forsling",
    "JerseyNumber": 42,
    "TeamId": 13,
    "Id": 326
  },
  {
    "Name": "Frank Vatrano",
    "JerseyNumber": 77,
    "TeamId": 13,
    "Id": 327
  },
  {
    "Name": "Noel Acciari",
    "JerseyNumber": 55,
    "TeamId": 13,
    "Id": 328
  },
  {
    "Name": "Markus Nutivaara",
    "JerseyNumber": 65,
    "TeamId": 13,
    "Id": 329
  },
  {
    "Name": "Mason Marchment",
    "JerseyNumber": 19,
    "TeamId": 13,
    "Id": 330
  },
  {
    "Name": "Ryan Lomberg",
    "JerseyNumber": 94,
    "TeamId": 13,
    "Id": 331
  },
  {
    "Name": "Owen Tippett",
    "JerseyNumber": 74,
    "TeamId": 13,
    "Id": 332
  },
  {
    "Name": "Eetu Luostarinen",
    "JerseyNumber": 27,
    "TeamId": 13,
    "Id": 333
  },
  {
    "Name": "Spencer Knight",
    "JerseyNumber": 30,
    "TeamId": 13,
    "Id": 334
  },
  {
    "Name": "Matt Kiersted",
    "JerseyNumber": 8,
    "TeamId": 13,
    "Id": 335
  },
  {
    "Name": "Steven Stamkos",
    "JerseyNumber": 91,
    "TeamId": 14,
    "Id": 336
  },
  {
    "Name": "Jan Rutta",
    "JerseyNumber": 44,
    "TeamId": 14,
    "Id": 337
  },
  {
    "Name": "Fredrik Claesson",
    "JerseyNumber": 3,
    "TeamId": 14,
    "Id": 338
  },
  {
    "Name": "Christopher Gibson",
    "JerseyNumber": 33,
    "TeamId": 14,
    "Id": 339
  },
  {
    "Name": "Ben Thomas",
    "JerseyNumber": 56,
    "TeamId": 14,
    "Id": 340
  },
  {
    "Name": "Daniel Walcott",
    "JerseyNumber": 85,
    "TeamId": 14,
    "Id": 341
  },
  {
    "Name": "Cal Foote",
    "JerseyNumber": 52,
    "TeamId": 14,
    "Id": 342
  },
  {
    "Name": "Curtis McElhinney",
    "JerseyNumber": 35,
    "TeamId": 14,
    "Id": 343
  },
  {
    "Name": "Alex Killorn",
    "JerseyNumber": 17,
    "TeamId": 14,
    "Id": 344
  },
  {
    "Name": "Pat Maroon",
    "JerseyNumber": 14,
    "TeamId": 14,
    "Id": 345
  },
  {
    "Name": "Ryan McDonagh",
    "JerseyNumber": 27,
    "TeamId": 14,
    "Id": 346
  },
  {
    "Name": "Luke Schenn",
    "JerseyNumber": 2,
    "TeamId": 14,
    "Id": 347
  },
  {
    "Name": "Tyler Johnson",
    "JerseyNumber": 9,
    "TeamId": 14,
    "Id": 348
  },
  {
    "Name": "Victor Hedman",
    "JerseyNumber": 77,
    "TeamId": 14,
    "Id": 349
  },
  {
    "Name": "David Savard",
    "JerseyNumber": 58,
    "TeamId": 14,
    "Id": 350
  },
  {
    "Name": "Ondrej Palat",
    "JerseyNumber": 18,
    "TeamId": 14,
    "Id": 351
  },
  {
    "Name": "Blake Coleman",
    "JerseyNumber": 20,
    "TeamId": 14,
    "Id": 352
  },
  {
    "Name": "Barclay Goodrow",
    "JerseyNumber": 19,
    "TeamId": 14,
    "Id": 353
  },
  {
    "Name": "Yanni Gourde",
    "JerseyNumber": 37,
    "TeamId": 14,
    "Id": 354
  },
  {
    "Name": "Andrei Vasilevskiy",
    "JerseyNumber": 88,
    "TeamId": 14,
    "Id": 355
  },
  {
    "Name": "Brayden Point",
    "JerseyNumber": 21,
    "TeamId": 14,
    "Id": 356
  },
  {
    "Name": "Erik Cernak",
    "JerseyNumber": 81,
    "TeamId": 14,
    "Id": 357
  },
  {
    "Name": "Mathieu Joseph",
    "JerseyNumber": 7,
    "TeamId": 14,
    "Id": 358
  },
  {
    "Name": "Mitchell Stephens",
    "JerseyNumber": 67,
    "TeamId": 14,
    "Id": 359
  },
  {
    "Name": "Anthony Cirelli",
    "JerseyNumber": 71,
    "TeamId": 14,
    "Id": 360
  },
  {
    "Name": "Mikhail Sergachev",
    "JerseyNumber": 98,
    "TeamId": 14,
    "Id": 361
  },
  {
    "Name": "Ross Colton",
    "JerseyNumber": 79,
    "TeamId": 14,
    "Id": 362
  },
  {
    "Name": "Alex Barre-Boulet",
    "JerseyNumber": 60,
    "TeamId": 14,
    "Id": 363
  },
  {
    "Name": "Craig Anderson",
    "JerseyNumber": 31,
    "TeamId": 15,
    "Id": 364
  },
  {
    "Name": "Michal Kempny",
    "JerseyNumber": 6,
    "TeamId": 15,
    "Id": 365
  },
  {
    "Name": "Zdeno Chara",
    "JerseyNumber": 33,
    "TeamId": 15,
    "Id": 366
  },
  {
    "Name": "Alex Ovechkin",
    "JerseyNumber": 8,
    "TeamId": 15,
    "Id": 367
  },
  {
    "Name": "T.J. Oshie",
    "JerseyNumber": 77,
    "TeamId": 15,
    "Id": 368
  },
  {
    "Name": "Nicklas Backstrom",
    "JerseyNumber": 19,
    "TeamId": 15,
    "Id": 369
  },
  {
    "Name": "Carl Hagelin",
    "JerseyNumber": 62,
    "TeamId": 15,
    "Id": 370
  },
  {
    "Name": "Lars Eller",
    "JerseyNumber": 20,
    "TeamId": 15,
    "Id": 371
  },
  {
    "Name": "John Carlson",
    "JerseyNumber": 74,
    "TeamId": 15,
    "Id": 372
  },
  {
    "Name": "Justin Schultz",
    "JerseyNumber": 2,
    "TeamId": 15,
    "Id": 373
  },
  {
    "Name": "Dmitry Orlov",
    "JerseyNumber": 9,
    "TeamId": 15,
    "Id": 374
  },
  {
    "Name": "Nick Jensen",
    "JerseyNumber": 3,
    "TeamId": 15,
    "Id": 375
  },
  {
    "Name": "Nic Dowd",
    "JerseyNumber": 26,
    "TeamId": 15,
    "Id": 376
  },
  {
    "Name": "Brenden Dillon",
    "JerseyNumber": 4,
    "TeamId": 15,
    "Id": 377
  },
  {
    "Name": "Evgeny Kuznetsov",
    "JerseyNumber": 92,
    "TeamId": 15,
    "Id": 378
  },
  {
    "Name": "Tom Wilson",
    "JerseyNumber": 43,
    "TeamId": 15,
    "Id": 379
  },
  {
    "Name": "Michael Raffl",
    "JerseyNumber": 17,
    "TeamId": 15,
    "Id": 380
  },
  {
    "Name": "Anthony Mantha",
    "JerseyNumber": 39,
    "TeamId": 15,
    "Id": 381
  },
  {
    "Name": "Conor Sheary",
    "JerseyNumber": 73,
    "TeamId": 15,
    "Id": 382
  },
  {
    "Name": "Trevor van Riemsdyk",
    "JerseyNumber": 57,
    "TeamId": 15,
    "Id": 383
  },
  {
    "Name": "Garnet Hathaway",
    "JerseyNumber": 21,
    "TeamId": 15,
    "Id": 384
  },
  {
    "Name": "Vitek Vanecek",
    "JerseyNumber": 41,
    "TeamId": 15,
    "Id": 385
  },
  {
    "Name": "Daniel Sprong",
    "JerseyNumber": 10,
    "TeamId": 15,
    "Id": 386
  },
  {
    "Name": "Ilya Samsonov",
    "JerseyNumber": 30,
    "TeamId": 15,
    "Id": 387
  },
  {
    "Name": "Brent Seabrook",
    "JerseyNumber": 7,
    "TeamId": 16,
    "Id": 388
  },
  {
    "Name": "Zack Smith",
    "JerseyNumber": 15,
    "TeamId": 16,
    "Id": 389
  },
  {
    "Name": "Andrew Shaw",
    "JerseyNumber": 65,
    "TeamId": 16,
    "Id": 390
  },
  {
    "Name": "Jonathan Toews",
    "JerseyNumber": 19,
    "TeamId": 16,
    "Id": 391
  },
  {
    "Name": "Alex Nylander",
    "JerseyNumber": 92,
    "TeamId": 16,
    "Id": 392
  },
  {
    "Name": "Duncan Keith",
    "JerseyNumber": 2,
    "TeamId": 16,
    "Id": 393
  },
  {
    "Name": "Patrick Kane",
    "JerseyNumber": 88,
    "TeamId": 16,
    "Id": 394
  },
  {
    "Name": "Calvin de Haan",
    "JerseyNumber": 44,
    "TeamId": 16,
    "Id": 395
  },
  {
    "Name": "Brett Connolly",
    "JerseyNumber": 20,
    "TeamId": 16,
    "Id": 396
  },
  {
    "Name": "Connor Murphy",
    "JerseyNumber": 5,
    "TeamId": 16,
    "Id": 397
  },
  {
    "Name": "Malcolm Subban",
    "JerseyNumber": 30,
    "TeamId": 16,
    "Id": 398
  },
  {
    "Name": "Vinnie Hinostroza",
    "JerseyNumber": 28,
    "TeamId": 16,
    "Id": 399
  },
  {
    "Name": "Dominik Kubalik",
    "JerseyNumber": 8,
    "TeamId": 16,
    "Id": 400
  },
  {
    "Name": "Nikita Zadorov",
    "JerseyNumber": 16,
    "TeamId": 16,
    "Id": 401
  },
  {
    "Name": "Ryan Carpenter",
    "JerseyNumber": 22,
    "TeamId": 16,
    "Id": 402
  },
  {
    "Name": "Dylan Strome",
    "JerseyNumber": 17,
    "TeamId": 16,
    "Id": 403
  },
  {
    "Name": "Adam Gaudette",
    "JerseyNumber": 11,
    "TeamId": 16,
    "Id": 404
  },
  {
    "Name": "Alex DeBrincat",
    "JerseyNumber": 12,
    "TeamId": 16,
    "Id": 405
  },
  {
    "Name": "Riley Stillman",
    "JerseyNumber": 61,
    "TeamId": 16,
    "Id": 406
  },
  {
    "Name": "Brandon Hagel",
    "JerseyNumber": 38,
    "TeamId": 16,
    "Id": 407
  },
  {
    "Name": "Ian Mitchell",
    "JerseyNumber": 51,
    "TeamId": 16,
    "Id": 408
  },
  {
    "Name": "David Kampf",
    "JerseyNumber": 64,
    "TeamId": 16,
    "Id": 409
  },
  {
    "Name": "Wyatt Kalynuk",
    "JerseyNumber": 48,
    "TeamId": 16,
    "Id": 410
  },
  {
    "Name": "Collin Delia",
    "JerseyNumber": 60,
    "TeamId": 16,
    "Id": 411
  },
  {
    "Name": "Pius Suter",
    "JerseyNumber": 24,
    "TeamId": 16,
    "Id": 412
  },
  {
    "Name": "Philipp Kurashev",
    "JerseyNumber": 23,
    "TeamId": 16,
    "Id": 413
  },
  {
    "Name": "Adam Boqvist",
    "JerseyNumber": 27,
    "TeamId": 16,
    "Id": 414
  },
  {
    "Name": "Kevin Lankinen",
    "JerseyNumber": 32,
    "TeamId": 16,
    "Id": 415
  },
  {
    "Name": "Kirby Dach",
    "JerseyNumber": 77,
    "TeamId": 16,
    "Id": 416
  },
  {
    "Name": "Mike Hardman",
    "JerseyNumber": 86,
    "TeamId": 16,
    "Id": 417
  },
  {
    "Name": "Bobby Ryan",
    "JerseyNumber": 54,
    "TeamId": 17,
    "Id": 418
  },
  {
    "Name": "Tyler Bertuzzi",
    "JerseyNumber": 59,
    "TeamId": 17,
    "Id": 419
  },
  {
    "Name": "Robby Fabbri",
    "JerseyNumber": 14,
    "TeamId": 17,
    "Id": 420
  },
  {
    "Name": "Mathias Brome",
    "JerseyNumber": 86,
    "TeamId": 17,
    "Id": 421
  },
  {
    "Name": "Valtteri Filppula",
    "JerseyNumber": 51,
    "TeamId": 17,
    "Id": 422
  },
  {
    "Name": "Thomas Greiss",
    "JerseyNumber": 29,
    "TeamId": 17,
    "Id": 423
  },
  {
    "Name": "Marc Staal",
    "JerseyNumber": 18,
    "TeamId": 17,
    "Id": 424
  },
  {
    "Name": "Darren Helm",
    "JerseyNumber": 43,
    "TeamId": 17,
    "Id": 425
  },
  {
    "Name": "Alex Biega",
    "JerseyNumber": 3,
    "TeamId": 17,
    "Id": 426
  },
  {
    "Name": "Jonathan Bernier",
    "JerseyNumber": 45,
    "TeamId": 17,
    "Id": 427
  },
  {
    "Name": "Sam Gagner",
    "JerseyNumber": 89,
    "TeamId": 17,
    "Id": 428
  },
  {
    "Name": "Richard Panik",
    "JerseyNumber": 24,
    "TeamId": 17,
    "Id": 429
  },
  {
    "Name": "Vladislav Namestnikov",
    "JerseyNumber": 92,
    "TeamId": 17,
    "Id": 430
  },
  {
    "Name": "Luke Glendening",
    "JerseyNumber": 41,
    "TeamId": 17,
    "Id": 431
  },
  {
    "Name": "Christian Djoos",
    "JerseyNumber": 44,
    "TeamId": 17,
    "Id": 432
  },
  {
    "Name": "Danny DeKeyser",
    "JerseyNumber": 65,
    "TeamId": 17,
    "Id": 433
  },
  {
    "Name": "Adam Erne",
    "JerseyNumber": 73,
    "TeamId": 17,
    "Id": 434
  },
  {
    "Name": "Jakub Vrana",
    "JerseyNumber": 15,
    "TeamId": 17,
    "Id": 435
  },
  {
    "Name": "Dylan Larkin",
    "JerseyNumber": 71,
    "TeamId": 17,
    "Id": 436
  },
  {
    "Name": "Evgeny Svechnikov",
    "JerseyNumber": 37,
    "TeamId": 17,
    "Id": 437
  },
  {
    "Name": "Dennis Cholowski",
    "JerseyNumber": 21,
    "TeamId": 17,
    "Id": 438
  },
  {
    "Name": "Filip Hronek",
    "JerseyNumber": 17,
    "TeamId": 17,
    "Id": 439
  },
  {
    "Name": "Troy Stecher",
    "JerseyNumber": 70,
    "TeamId": 17,
    "Id": 440
  },
  {
    "Name": "Michael Rasmussen",
    "JerseyNumber": 27,
    "TeamId": 17,
    "Id": 441
  },
  {
    "Name": "Gustav Lindstrom",
    "JerseyNumber": 28,
    "TeamId": 17,
    "Id": 442
  },
  {
    "Name": "Filip Zadina",
    "JerseyNumber": 11,
    "TeamId": 17,
    "Id": 443
  },
  {
    "Name": "Brad Richardson",
    "JerseyNumber": 15,
    "TeamId": 18,
    "Id": 444
  },
  {
    "Name": "Luca Sbisa",
    "JerseyNumber": 55,
    "TeamId": 18,
    "Id": 445
  },
  {
    "Name": "Mark Borowiecki",
    "JerseyNumber": 90,
    "TeamId": 18,
    "Id": 446
  },
  {
    "Name": "Matt Duchene",
    "JerseyNumber": 95,
    "TeamId": 18,
    "Id": 447
  },
  {
    "Name": "Filip Forsberg",
    "JerseyNumber": 9,
    "TeamId": 18,
    "Id": 448
  },
  {
    "Name": "Alexandre Carrier",
    "JerseyNumber": 45,
    "TeamId": 18,
    "Id": 449
  },
  {
    "Name": "Mathieu Olivier",
    "JerseyNumber": 25,
    "TeamId": 18,
    "Id": 450
  },
  {
    "Name": "Tyler Lewington",
    "JerseyNumber": 2,
    "TeamId": 18,
    "Id": 451
  },
  {
    "Name": "Michael McCarron",
    "JerseyNumber": 47,
    "TeamId": 18,
    "Id": 452
  },
  {
    "Name": "Kasimir Kaskisuo",
    "JerseyNumber": 73,
    "TeamId": 18,
    "Id": 453
  },
  {
    "Name": "Rem Pitlick",
    "JerseyNumber": 16,
    "TeamId": 18,
    "Id": 454
  },
  {
    "Name": "Tanner Jeannot",
    "JerseyNumber": 84,
    "TeamId": 18,
    "Id": 455
  },
  {
    "Name": "Philip Tomasino",
    "JerseyNumber": 26,
    "TeamId": 18,
    "Id": 456
  },
  {
    "Name": "Pekka Rinne",
    "JerseyNumber": 35,
    "TeamId": 18,
    "Id": 457
  },
  {
    "Name": "Roman Josi",
    "JerseyNumber": 59,
    "TeamId": 18,
    "Id": 458
  },
  {
    "Name": "Ryan Ellis",
    "JerseyNumber": 4,
    "TeamId": 18,
    "Id": 459
  },
  {
    "Name": "Mattias Ekholm",
    "JerseyNumber": 14,
    "TeamId": 18,
    "Id": 460
  },
  {
    "Name": "Erik Haula",
    "JerseyNumber": 56,
    "TeamId": 18,
    "Id": 461
  },
  {
    "Name": "Calle Jarnkrok",
    "JerseyNumber": 19,
    "TeamId": 18,
    "Id": 462
  },
  {
    "Name": "Erik Gudbranson",
    "JerseyNumber": 44,
    "TeamId": 18,
    "Id": 463
  },
  {
    "Name": "Ryan Johansen",
    "JerseyNumber": 92,
    "TeamId": 18,
    "Id": 464
  },
  {
    "Name": "Mikael Granlund",
    "JerseyNumber": 64,
    "TeamId": 18,
    "Id": 465
  },
  {
    "Name": "Nick Cousins",
    "JerseyNumber": 21,
    "TeamId": 18,
    "Id": 466
  },
  {
    "Name": "Rocco Grimaldi",
    "JerseyNumber": 23,
    "TeamId": 18,
    "Id": 467
  },
  {
    "Name": "Colton Sissons",
    "JerseyNumber": 10,
    "TeamId": 18,
    "Id": 468
  },
  {
    "Name": "Matt Benning",
    "JerseyNumber": 5,
    "TeamId": 18,
    "Id": 469
  },
  {
    "Name": "Juuse Saros",
    "JerseyNumber": 74,
    "TeamId": 18,
    "Id": 470
  },
  {
    "Name": "Ben Harpur",
    "JerseyNumber": 17,
    "TeamId": 18,
    "Id": 471
  },
  {
    "Name": "Viktor Arvidsson",
    "JerseyNumber": 33,
    "TeamId": 18,
    "Id": 472
  },
  {
    "Name": "Yakov Trenin",
    "JerseyNumber": 13,
    "TeamId": 18,
    "Id": 473
  },
  {
    "Name": "Luke Kunin",
    "JerseyNumber": 11,
    "TeamId": 18,
    "Id": 474
  },
  {
    "Name": "Dante Fabbro",
    "JerseyNumber": 57,
    "TeamId": 18,
    "Id": 475
  },
  {
    "Name": "Jeremy Davies",
    "JerseyNumber": 38,
    "TeamId": 18,
    "Id": 476
  },
  {
    "Name": "Eeli Tolvanen",
    "JerseyNumber": 28,
    "TeamId": 18,
    "Id": 477
  },
  {
    "Name": "David Farrance",
    "JerseyNumber": 22,
    "TeamId": 18,
    "Id": 478
  },
  {
    "Name": "Carl Gunnarsson",
    "JerseyNumber": 4,
    "TeamId": 19,
    "Id": 479
  },
  {
    "Name": "Oskar Sundqvist",
    "JerseyNumber": 70,
    "TeamId": 19,
    "Id": 480
  },
  {
    "Name": "Mackenzie MacEachern",
    "JerseyNumber": 28,
    "TeamId": 19,
    "Id": 481
  },
  {
    "Name": "David Perron",
    "JerseyNumber": 57,
    "TeamId": 19,
    "Id": 482
  },
  {
    "Name": "Robert Bortuzzo",
    "JerseyNumber": 41,
    "TeamId": 19,
    "Id": 483
  },
  {
    "Name": "Marco Scandella",
    "JerseyNumber": 6,
    "TeamId": 19,
    "Id": 484
  },
  {
    "Name": "Mike Hoffman",
    "JerseyNumber": 68,
    "TeamId": 19,
    "Id": 485
  },
  {
    "Name": "Tyler Bozak",
    "JerseyNumber": 21,
    "TeamId": 19,
    "Id": 486
  },
  {
    "Name": "Ryan O\u0027Reilly",
    "JerseyNumber": 90,
    "TeamId": 19,
    "Id": 487
  },
  {
    "Name": "Kyle Clifford",
    "JerseyNumber": 13,
    "TeamId": 19,
    "Id": 488
  },
  {
    "Name": "Brayden Schenn",
    "JerseyNumber": 10,
    "TeamId": 19,
    "Id": 489
  },
  {
    "Name": "Justin Faulk",
    "JerseyNumber": 72,
    "TeamId": 19,
    "Id": 490
  },
  {
    "Name": "Vladimir Tarasenko",
    "JerseyNumber": 91,
    "TeamId": 19,
    "Id": 491
  },
  {
    "Name": "Jaden Schwartz",
    "JerseyNumber": 17,
    "TeamId": 19,
    "Id": 492
  },
  {
    "Name": "Jordan Binnington",
    "JerseyNumber": 50,
    "TeamId": 19,
    "Id": 493
  },
  {
    "Name": "Torey Krug",
    "JerseyNumber": 47,
    "TeamId": 19,
    "Id": 494
  },
  {
    "Name": "Colton Parayko",
    "JerseyNumber": 55,
    "TeamId": 19,
    "Id": 495
  },
  {
    "Name": "Zach Sanford",
    "JerseyNumber": 12,
    "TeamId": 19,
    "Id": 496
  },
  {
    "Name": "Ivan Barbashev",
    "JerseyNumber": 49,
    "TeamId": 19,
    "Id": 497
  },
  {
    "Name": "Jake Walman",
    "JerseyNumber": 46,
    "TeamId": 19,
    "Id": 498
  },
  {
    "Name": "Ville Husso",
    "JerseyNumber": 35,
    "TeamId": 19,
    "Id": 499
  },
  {
    "Name": "Sammy Blais",
    "JerseyNumber": 9,
    "TeamId": 19,
    "Id": 500
  },
  {
    "Name": "Vince Dunn",
    "JerseyNumber": 29,
    "TeamId": 19,
    "Id": 501
  },
  {
    "Name": "Niko Mikkola",
    "JerseyNumber": 77,
    "TeamId": 19,
    "Id": 502
  },
  {
    "Name": "Jordan Kyrou",
    "JerseyNumber": 25,
    "TeamId": 19,
    "Id": 503
  },
  {
    "Name": "Robert Thomas",
    "JerseyNumber": 18,
    "TeamId": 19,
    "Id": 504
  },
  {
    "Name": "Zac Rinaldo",
    "JerseyNumber": 36,
    "TeamId": 20,
    "Id": 505
  },
  {
    "Name": "Alexander Petrovic",
    "JerseyNumber": 15,
    "TeamId": 20,
    "Id": 506
  },
  {
    "Name": "Oliver Kylington",
    "JerseyNumber": 58,
    "TeamId": 20,
    "Id": 507
  },
  {
    "Name": "Dominik Simon",
    "JerseyNumber": 81,
    "TeamId": 20,
    "Id": 508
  },
  {
    "Name": "Adam Ruzicka",
    "JerseyNumber": 63,
    "TeamId": 20,
    "Id": 509
  },
  {
    "Name": "Artyom Zagidulin",
    "JerseyNumber": 50,
    "TeamId": 20,
    "Id": 510
  },
  {
    "Name": "Mark Giordano",
    "JerseyNumber": 5,
    "TeamId": 20,
    "Id": 511
  },
  {
    "Name": "Milan Lucic",
    "JerseyNumber": 17,
    "TeamId": 20,
    "Id": 512
  },
  {
    "Name": "Mikael Backlund",
    "JerseyNumber": 11,
    "TeamId": 20,
    "Id": 513
  },
  {
    "Name": "Jacob Markstrom",
    "JerseyNumber": 25,
    "TeamId": 20,
    "Id": 514
  },
  {
    "Name": "Michael Stone",
    "JerseyNumber": 26,
    "TeamId": 20,
    "Id": 515
  },
  {
    "Name": "Christopher Tanev",
    "JerseyNumber": 8,
    "TeamId": 20,
    "Id": 516
  },
  {
    "Name": "Joakim Nordstrom",
    "JerseyNumber": 20,
    "TeamId": 20,
    "Id": 517
  },
  {
    "Name": "Louis Domingue",
    "JerseyNumber": 70,
    "TeamId": 20,
    "Id": 518
  },
  {
    "Name": "Nikita Nesterov",
    "JerseyNumber": 89,
    "TeamId": 20,
    "Id": 519
  },
  {
    "Name": "Johnny Gaudreau",
    "JerseyNumber": 13,
    "TeamId": 20,
    "Id": 520
  },
  {
    "Name": "Josh Leivo",
    "JerseyNumber": 27,
    "TeamId": 20,
    "Id": 521
  },
  {
    "Name": "Brett Ritchie",
    "JerseyNumber": 24,
    "TeamId": 20,
    "Id": 522
  },
  {
    "Name": "Buddy Robinson",
    "JerseyNumber": 53,
    "TeamId": 20,
    "Id": 523
  },
  {
    "Name": "Elias Lindholm",
    "JerseyNumber": 28,
    "TeamId": 20,
    "Id": 524
  },
  {
    "Name": "Sean Monahan",
    "JerseyNumber": 23,
    "TeamId": 20,
    "Id": 525
  },
  {
    "Name": "Andrew Mangiapane",
    "JerseyNumber": 88,
    "TeamId": 20,
    "Id": 526
  },
  {
    "Name": "Noah Hanifin",
    "JerseyNumber": 55,
    "TeamId": 20,
    "Id": 527
  },
  {
    "Name": "Rasmus Andersson",
    "JerseyNumber": 4,
    "TeamId": 20,
    "Id": 528
  },
  {
    "Name": "Derek Ryan",
    "JerseyNumber": 10,
    "TeamId": 20,
    "Id": 529
  },
  {
    "Name": "Matthew Tkachuk",
    "JerseyNumber": 19,
    "TeamId": 20,
    "Id": 530
  },
  {
    "Name": "Dillon Dube",
    "JerseyNumber": 29,
    "TeamId": 20,
    "Id": 531
  },
  {
    "Name": "Juuso Valimaki",
    "JerseyNumber": 6,
    "TeamId": 20,
    "Id": 532
  },
  {
    "Name": "Erik Johnson",
    "JerseyNumber": 6,
    "TeamId": 21,
    "Id": 533
  },
  {
    "Name": "Matt Calvert",
    "JerseyNumber": 11,
    "TeamId": 21,
    "Id": 534
  },
  {
    "Name": "Pavel Francouz",
    "JerseyNumber": 39,
    "TeamId": 21,
    "Id": 535
  },
  {
    "Name": "Logan O\u0027Connor",
    "JerseyNumber": 25,
    "TeamId": 21,
    "Id": 536
  },
  {
    "Name": "Jayson Megna",
    "JerseyNumber": 12,
    "TeamId": 21,
    "Id": 537
  },
  {
    "Name": "Kyle Burroughs",
    "JerseyNumber": 88,
    "TeamId": 21,
    "Id": 538
  },
  {
    "Name": "Jonas Johansson",
    "JerseyNumber": 35,
    "TeamId": 21,
    "Id": 539
  },
  {
    "Name": "Dan Renouf",
    "JerseyNumber": 2,
    "TeamId": 21,
    "Id": 540
  },
  {
    "Name": "Conor Timmins",
    "JerseyNumber": 22,
    "TeamId": 21,
    "Id": 541
  },
  {
    "Name": "Kiefer Sherwood",
    "JerseyNumber": 44,
    "TeamId": 21,
    "Id": 542
  },
  {
    "Name": "Bowen Byram",
    "JerseyNumber": 4,
    "TeamId": 21,
    "Id": 543
  },
  {
    "Name": "Peyton Jones",
    "JerseyNumber": 42,
    "TeamId": 21,
    "Id": 544
  },
  {
    "Name": "Devan Dubnyk",
    "JerseyNumber": 40,
    "TeamId": 21,
    "Id": 545
  },
  {
    "Name": "Carl Soderberg",
    "JerseyNumber": 34,
    "TeamId": 21,
    "Id": 546
  },
  {
    "Name": "Nazem Kadri",
    "JerseyNumber": 91,
    "TeamId": 21,
    "Id": 547
  },
  {
    "Name": "Patrik Nemeth",
    "JerseyNumber": 24,
    "TeamId": 21,
    "Id": 548
  },
  {
    "Name": "Joonas Donskoi",
    "JerseyNumber": 72,
    "TeamId": 21,
    "Id": 549
  },
  {
    "Name": "Philipp Grubauer",
    "JerseyNumber": 31,
    "TeamId": 21,
    "Id": 550
  },
  {
    "Name": "Brandon Saad",
    "JerseyNumber": 20,
    "TeamId": 21,
    "Id": 551
  },
  {
    "Name": "Gabriel Landeskog",
    "JerseyNumber": 92,
    "TeamId": 21,
    "Id": 552
  },
  {
    "Name": "Liam O\u0027Brien",
    "JerseyNumber": 38,
    "TeamId": 21,
    "Id": 553
  },
  {
    "Name": "Ryan Graves",
    "JerseyNumber": 27,
    "TeamId": 21,
    "Id": 554
  },
  {
    "Name": "Andre Burakovsky",
    "JerseyNumber": 95,
    "TeamId": 21,
    "Id": 555
  },
  {
    "Name": "J.T. Compher",
    "JerseyNumber": 37,
    "TeamId": 21,
    "Id": 556
  },
  {
    "Name": "Nathan MacKinnon",
    "JerseyNumber": 29,
    "TeamId": 21,
    "Id": 557
  },
  {
    "Name": "Valeri Nichushkin",
    "JerseyNumber": 13,
    "TeamId": 21,
    "Id": 558
  },
  {
    "Name": "Pierre-Edouard Bellemare",
    "JerseyNumber": 41,
    "TeamId": 21,
    "Id": 559
  },
  {
    "Name": "Devon Toews",
    "JerseyNumber": 7,
    "TeamId": 21,
    "Id": 560
  },
  {
    "Name": "Mikko Rantanen",
    "JerseyNumber": 96,
    "TeamId": 21,
    "Id": 561
  },
  {
    "Name": "Tyson Jost",
    "JerseyNumber": 17,
    "TeamId": 21,
    "Id": 562
  },
  {
    "Name": "Samuel Girard",
    "JerseyNumber": 49,
    "TeamId": 21,
    "Id": 563
  },
  {
    "Name": "Jacob MacDonald",
    "JerseyNumber": 26,
    "TeamId": 21,
    "Id": 564
  },
  {
    "Name": "Cale Makar",
    "JerseyNumber": 8,
    "TeamId": 21,
    "Id": 565
  },
  {
    "Name": "Ryan Nugent-Hopkins",
    "JerseyNumber": 93,
    "TeamId": 22,
    "Id": 566
  },
  {
    "Name": "Slater Koekkoek",
    "JerseyNumber": 20,
    "TeamId": 22,
    "Id": 567
  },
  {
    "Name": "James Neal",
    "JerseyNumber": 18,
    "TeamId": 22,
    "Id": 568
  },
  {
    "Name": "Kyle Turris",
    "JerseyNumber": 8,
    "TeamId": 22,
    "Id": 569
  },
  {
    "Name": "Tyler Ennis",
    "JerseyNumber": 63,
    "TeamId": 22,
    "Id": 570
  },
  {
    "Name": "Patrick Russell",
    "JerseyNumber": 52,
    "TeamId": 22,
    "Id": 571
  },
  {
    "Name": "Evan Bouchard",
    "JerseyNumber": 75,
    "TeamId": 22,
    "Id": 572
  },
  {
    "Name": "Joakim Nygard",
    "JerseyNumber": 10,
    "TeamId": 22,
    "Id": 573
  },
  {
    "Name": "Mike Smith",
    "JerseyNumber": 41,
    "TeamId": 22,
    "Id": 574
  },
  {
    "Name": "Kris Russell",
    "JerseyNumber": 4,
    "TeamId": 22,
    "Id": 575
  },
  {
    "Name": "Alex Stalock",
    "JerseyNumber": 32,
    "TeamId": 22,
    "Id": 576
  },
  {
    "Name": "Mikko Koskinen",
    "JerseyNumber": 19,
    "TeamId": 22,
    "Id": 577
  },
  {
    "Name": "Alex Chiasson",
    "JerseyNumber": 39,
    "TeamId": 22,
    "Id": 578
  },
  {
    "Name": "Zack Kassian",
    "JerseyNumber": 44,
    "TeamId": 22,
    "Id": 579
  },
  {
    "Name": "Dmitry Kulikov",
    "JerseyNumber": 70,
    "TeamId": 22,
    "Id": 580
  },
  {
    "Name": "Tyson Barrie",
    "JerseyNumber": 22,
    "TeamId": 22,
    "Id": 581
  },
  {
    "Name": "Josh Archibald",
    "JerseyNumber": 15,
    "TeamId": 22,
    "Id": 582
  },
  {
    "Name": "Adam Larsson",
    "JerseyNumber": 6,
    "TeamId": 22,
    "Id": 583
  },
  {
    "Name": "Devin Shore",
    "JerseyNumber": 14,
    "TeamId": 22,
    "Id": 584
  },
  {
    "Name": "Jujhar Khaira",
    "JerseyNumber": 16,
    "TeamId": 22,
    "Id": 585
  },
  {
    "Name": "Darnell Nurse",
    "JerseyNumber": 25,
    "TeamId": 22,
    "Id": 586
  },
  {
    "Name": "Leon Draisaitl",
    "JerseyNumber": 29,
    "TeamId": 22,
    "Id": 587
  },
  {
    "Name": "William Lagesson",
    "JerseyNumber": 84,
    "TeamId": 22,
    "Id": 588
  },
  {
    "Name": "Connor McDavid",
    "JerseyNumber": 97,
    "TeamId": 22,
    "Id": 589
  },
  {
    "Name": "Ethan Bear",
    "JerseyNumber": 74,
    "TeamId": 22,
    "Id": 590
  },
  {
    "Name": "Caleb Jones",
    "JerseyNumber": 82,
    "TeamId": 22,
    "Id": 591
  },
  {
    "Name": "Jesse Puljujarvi",
    "JerseyNumber": 13,
    "TeamId": 22,
    "Id": 592
  },
  {
    "Name": "Kailer Yamamoto",
    "JerseyNumber": 56,
    "TeamId": 22,
    "Id": 593
  },
  {
    "Name": "Dominik Kahun",
    "JerseyNumber": 21,
    "TeamId": 22,
    "Id": 594
  },
  {
    "Name": "Gaetan Haas",
    "JerseyNumber": 91,
    "TeamId": 22,
    "Id": 595
  },
  {
    "Name": "Jay Beagle",
    "JerseyNumber": 83,
    "TeamId": 23,
    "Id": 596
  },
  {
    "Name": "Tanner Pearson",
    "JerseyNumber": 70,
    "TeamId": 23,
    "Id": 597
  },
  {
    "Name": "Justin Bailey",
    "JerseyNumber": 95,
    "TeamId": 23,
    "Id": 598
  },
  {
    "Name": "Elias Pettersson",
    "JerseyNumber": 40,
    "TeamId": 23,
    "Id": 599
  },
  {
    "Name": "Alexander Edler",
    "JerseyNumber": 23,
    "TeamId": 23,
    "Id": 600
  },
  {
    "Name": "Brandon Sutter",
    "JerseyNumber": 20,
    "TeamId": 23,
    "Id": 601
  },
  {
    "Name": "Tyler Myers",
    "JerseyNumber": 57,
    "TeamId": 23,
    "Id": 602
  },
  {
    "Name": "Travis Hamonic",
    "JerseyNumber": 27,
    "TeamId": 23,
    "Id": 603
  },
  {
    "Name": "Braden Holtby",
    "JerseyNumber": 49,
    "TeamId": 23,
    "Id": 604
  },
  {
    "Name": "Antoine Roussel",
    "JerseyNumber": 26,
    "TeamId": 23,
    "Id": 605
  },
  {
    "Name": "Travis Boyd",
    "JerseyNumber": 72,
    "TeamId": 23,
    "Id": 606
  },
  {
    "Name": "J.T. Miller",
    "JerseyNumber": 9,
    "TeamId": 23,
    "Id": 607
  },
  {
    "Name": "Jimmy Vesey",
    "JerseyNumber": 24,
    "TeamId": 23,
    "Id": 608
  },
  {
    "Name": "Ashton Sautner",
    "JerseyNumber": 29,
    "TeamId": 23,
    "Id": 609
  },
  {
    "Name": "Nate Schmidt",
    "JerseyNumber": 88,
    "TeamId": 23,
    "Id": 610
  },
  {
    "Name": "Tyler Motte",
    "JerseyNumber": 64,
    "TeamId": 23,
    "Id": 611
  },
  {
    "Name": "Bo Horvat",
    "JerseyNumber": 53,
    "TeamId": 23,
    "Id": 612
  },
  {
    "Name": "Jake Virtanen",
    "JerseyNumber": 18,
    "TeamId": 23,
    "Id": 613
  },
  {
    "Name": "Jayce Hawryluk",
    "JerseyNumber": 13,
    "TeamId": 23,
    "Id": 614
  },
  {
    "Name": "Thatcher Demko",
    "JerseyNumber": 35,
    "TeamId": 23,
    "Id": 615
  },
  {
    "Name": "Matthew Highmore",
    "JerseyNumber": 36,
    "TeamId": 23,
    "Id": 616
  },
  {
    "Name": "Brock Boeser",
    "JerseyNumber": 6,
    "TeamId": 23,
    "Id": 617
  },
  {
    "Name": "Jalen Chatfield",
    "JerseyNumber": 63,
    "TeamId": 23,
    "Id": 618
  },
  {
    "Name": "Zack MacEwen",
    "JerseyNumber": 71,
    "TeamId": 23,
    "Id": 619
  },
  {
    "Name": "Quinn Hughes",
    "JerseyNumber": 43,
    "TeamId": 23,
    "Id": 620
  },
  {
    "Name": "Brogan Rafferty",
    "JerseyNumber": 25,
    "TeamId": 23,
    "Id": 621
  },
  {
    "Name": "Nils Hoglander",
    "JerseyNumber": 36,
    "TeamId": 23,
    "Id": 622
  },
  {
    "Name": "Marc Michaelis",
    "JerseyNumber": 56,
    "TeamId": 23,
    "Id": 623
  },
  {
    "Name": "Hampus Lindholm",
    "JerseyNumber": 47,
    "TeamId": 24,
    "Id": 624
  },
  {
    "Name": "Carter Rowney",
    "JerseyNumber": 24,
    "TeamId": 24,
    "Id": 625
  },
  {
    "Name": "Sonny Milano",
    "JerseyNumber": 12,
    "TeamId": 24,
    "Id": 626
  },
  {
    "Name": "Andrew Agozzino",
    "JerseyNumber": 26,
    "TeamId": 24,
    "Id": 627
  },
  {
    "Name": "Sam Carrick",
    "JerseyNumber": 39,
    "TeamId": 24,
    "Id": 628
  },
  {
    "Name": "Chase De Leo",
    "JerseyNumber": 58,
    "TeamId": 24,
    "Id": 629
  },
  {
    "Name": "Trevor Zegras",
    "JerseyNumber": 46,
    "TeamId": 24,
    "Id": 630
  },
  {
    "Name": "Ryan Miller",
    "JerseyNumber": 30,
    "TeamId": 24,
    "Id": 631
  },
  {
    "Name": "Ryan Getzlaf",
    "JerseyNumber": 15,
    "TeamId": 24,
    "Id": 632
  },
  {
    "Name": "Kevin Shattenkirk",
    "JerseyNumber": 22,
    "TeamId": 24,
    "Id": 633
  },
  {
    "Name": "Adam Henrique",
    "JerseyNumber": 14,
    "TeamId": 24,
    "Id": 634
  },
  {
    "Name": "Derek Grant",
    "JerseyNumber": 38,
    "TeamId": 24,
    "Id": 635
  },
  {
    "Name": "Jakob Silfverberg",
    "JerseyNumber": 33,
    "TeamId": 24,
    "Id": 636
  },
  {
    "Name": "Nicolas Deslauriers",
    "JerseyNumber": 20,
    "TeamId": 24,
    "Id": 637
  },
  {
    "Name": "Cam Fowler",
    "JerseyNumber": 4,
    "TeamId": 24,
    "Id": 638
  },
  {
    "Name": "Josh Manson",
    "JerseyNumber": 42,
    "TeamId": 24,
    "Id": 639
  },
  {
    "Name": "Andy Welinski",
    "JerseyNumber": 45,
    "TeamId": 24,
    "Id": 640
  },
  {
    "Name": "John Gibson",
    "JerseyNumber": 36,
    "TeamId": 24,
    "Id": 641
  },
  {
    "Name": "Rickard Rakell",
    "JerseyNumber": 67,
    "TeamId": 24,
    "Id": 642
  },
  {
    "Name": "Anthony Stolarz",
    "JerseyNumber": 41,
    "TeamId": 24,
    "Id": 643
  },
  {
    "Name": "Haydn Fleury",
    "JerseyNumber": 51,
    "TeamId": 24,
    "Id": 644
  },
  {
    "Name": "Danton Heinen",
    "JerseyNumber": 43,
    "TeamId": 24,
    "Id": 645
  },
  {
    "Name": "Jacob Larsson",
    "JerseyNumber": 32,
    "TeamId": 24,
    "Id": 646
  },
  {
    "Name": "Troy Terry",
    "JerseyNumber": 61,
    "TeamId": 24,
    "Id": 647
  },
  {
    "Name": "Sam Steel",
    "JerseyNumber": 23,
    "TeamId": 24,
    "Id": 648
  },
  {
    "Name": "Max Jones",
    "JerseyNumber": 49,
    "TeamId": 24,
    "Id": 649
  },
  {
    "Name": "Max Comtois",
    "JerseyNumber": 53,
    "TeamId": 24,
    "Id": 650
  },
  {
    "Name": "Alexander Volkov",
    "JerseyNumber": 92,
    "TeamId": 24,
    "Id": 651
  },
  {
    "Name": "Isac Lundestrom",
    "JerseyNumber": 48,
    "TeamId": 24,
    "Id": 652
  },
  {
    "Name": "Jamie Drysdale",
    "JerseyNumber": 34,
    "TeamId": 24,
    "Id": 653
  },
  {
    "Name": "Alexander Radulov",
    "JerseyNumber": 47,
    "TeamId": 25,
    "Id": 654
  },
  {
    "Name": "Joel Kiviranta",
    "JerseyNumber": 25,
    "TeamId": 25,
    "Id": 655
  },
  {
    "Name": "Ben Bishop",
    "JerseyNumber": 30,
    "TeamId": 25,
    "Id": 656
  },
  {
    "Name": "Stephen Johns",
    "JerseyNumber": 28,
    "TeamId": 25,
    "Id": 657
  },
  {
    "Name": "Tyler Seguin",
    "JerseyNumber": 91,
    "TeamId": 25,
    "Id": 658
  },
  {
    "Name": "Taylor Fedun",
    "JerseyNumber": 42,
    "TeamId": 25,
    "Id": 659
  },
  {
    "Name": "Joel L\u0027Esperance",
    "JerseyNumber": 38,
    "TeamId": 25,
    "Id": 660
  },
  {
    "Name": "Joe Pavelski",
    "JerseyNumber": 16,
    "TeamId": 25,
    "Id": 661
  },
  {
    "Name": "Blake Comeau",
    "JerseyNumber": 15,
    "TeamId": 25,
    "Id": 662
  },
  {
    "Name": "Andrej Sekera",
    "JerseyNumber": 5,
    "TeamId": 25,
    "Id": 663
  },
  {
    "Name": "Anton Khudobin",
    "JerseyNumber": 35,
    "TeamId": 25,
    "Id": 664
  },
  {
    "Name": "Andrew Cogliano",
    "JerseyNumber": 11,
    "TeamId": 25,
    "Id": 665
  },
  {
    "Name": "Jamie Benn",
    "JerseyNumber": 14,
    "TeamId": 25,
    "Id": 666
  },
  {
    "Name": "Sami Vatanen",
    "JerseyNumber": 45,
    "TeamId": 25,
    "Id": 667
  },
  {
    "Name": "Justin Dowling",
    "JerseyNumber": 37,
    "TeamId": 25,
    "Id": 668
  },
  {
    "Name": "Mark Pysyk",
    "JerseyNumber": 13,
    "TeamId": 25,
    "Id": 669
  },
  {
    "Name": "John Klingberg",
    "JerseyNumber": 3,
    "TeamId": 25,
    "Id": 670
  },
  {
    "Name": "Jamie Oleksiak",
    "JerseyNumber": 2,
    "TeamId": 25,
    "Id": 671
  },
  {
    "Name": "Radek Faksa",
    "JerseyNumber": 12,
    "TeamId": 25,
    "Id": 672
  },
  {
    "Name": "Esa Lindell",
    "JerseyNumber": 23,
    "TeamId": 25,
    "Id": 673
  },
  {
    "Name": "Jason Dickinson",
    "JerseyNumber": 18,
    "TeamId": 25,
    "Id": 674
  },
  {
    "Name": "Joel Hanley",
    "JerseyNumber": 44,
    "TeamId": 25,
    "Id": 675
  },
  {
    "Name": "Roope Hintz",
    "JerseyNumber": 24,
    "TeamId": 25,
    "Id": 676
  },
  {
    "Name": "Denis Gurianov",
    "JerseyNumber": 34,
    "TeamId": 25,
    "Id": 677
  },
  {
    "Name": "Tanner Kero",
    "JerseyNumber": 64,
    "TeamId": 25,
    "Id": 678
  },
  {
    "Name": "Rhett Gardner",
    "JerseyNumber": 49,
    "TeamId": 25,
    "Id": 679
  },
  {
    "Name": "Jake Oettinger",
    "JerseyNumber": 29,
    "TeamId": 25,
    "Id": 680
  },
  {
    "Name": "Jason Robertson",
    "JerseyNumber": 21,
    "TeamId": 25,
    "Id": 681
  },
  {
    "Name": "Miro Heiskanen",
    "JerseyNumber": 4,
    "TeamId": 25,
    "Id": 682
  },
  {
    "Name": "Martin Frk",
    "JerseyNumber": 29,
    "TeamId": 26,
    "Id": 683
  },
  {
    "Name": "Troy Grosenick",
    "JerseyNumber": 1,
    "TeamId": 26,
    "Id": 684
  },
  {
    "Name": "Lias Andersson",
    "JerseyNumber": 24,
    "TeamId": 26,
    "Id": 685
  },
  {
    "Name": "Cole Hults",
    "JerseyNumber": 54,
    "TeamId": 26,
    "Id": 686
  },
  {
    "Name": "Drake Rymsha",
    "JerseyNumber": 43,
    "TeamId": 26,
    "Id": 687
  },
  {
    "Name": "Austin Strand",
    "JerseyNumber": 71,
    "TeamId": 26,
    "Id": 688
  },
  {
    "Name": "Dustin Brown",
    "JerseyNumber": 23,
    "TeamId": 26,
    "Id": 689
  },
  {
    "Name": "Anze Kopitar",
    "JerseyNumber": 11,
    "TeamId": 26,
    "Id": 690
  },
  {
    "Name": "Jonathan Quick",
    "JerseyNumber": 32,
    "TeamId": 26,
    "Id": 691
  },
  {
    "Name": "Drew Doughty",
    "JerseyNumber": 8,
    "TeamId": 26,
    "Id": 692
  },
  {
    "Name": "Olli Maatta",
    "JerseyNumber": 6,
    "TeamId": 26,
    "Id": 693
  },
  {
    "Name": "Andreas Athanasiou",
    "JerseyNumber": 22,
    "TeamId": 26,
    "Id": 694
  },
  {
    "Name": "Kurtis MacDermid",
    "JerseyNumber": 56,
    "TeamId": 26,
    "Id": 695
  },
  {
    "Name": "Calvin Petersen",
    "JerseyNumber": 40,
    "TeamId": 26,
    "Id": 696
  },
  {
    "Name": "Adrian Kempe",
    "JerseyNumber": 9,
    "TeamId": 26,
    "Id": 697
  },
  {
    "Name": "Brendan Lemieux",
    "JerseyNumber": 48,
    "TeamId": 26,
    "Id": 698
  },
  {
    "Name": "Austin Wagner",
    "JerseyNumber": 27,
    "TeamId": 26,
    "Id": 699
  },
  {
    "Name": "Christian Wolanin",
    "JerseyNumber": 21,
    "TeamId": 26,
    "Id": 700
  },
  {
    "Name": "Carl Grundstrom",
    "JerseyNumber": 91,
    "TeamId": 26,
    "Id": 701
  },
  {
    "Name": "Kale Clague",
    "JerseyNumber": 58,
    "TeamId": 26,
    "Id": 702
  },
  {
    "Name": "Matt Luff",
    "JerseyNumber": 64,
    "TeamId": 26,
    "Id": 703
  },
  {
    "Name": "Trevor Moore",
    "JerseyNumber": 12,
    "TeamId": 26,
    "Id": 704
  },
  {
    "Name": "Jaret Anderson-Dolan",
    "JerseyNumber": 28,
    "TeamId": 26,
    "Id": 705
  },
  {
    "Name": "Mikey Anderson",
    "JerseyNumber": 44,
    "TeamId": 26,
    "Id": 706
  },
  {
    "Name": "Gabriel Vilardi",
    "JerseyNumber": 13,
    "TeamId": 26,
    "Id": 707
  },
  {
    "Name": "Alex Iafallo",
    "JerseyNumber": 19,
    "TeamId": 26,
    "Id": 708
  },
  {
    "Name": "Sean Walker",
    "JerseyNumber": 26,
    "TeamId": 26,
    "Id": 709
  },
  {
    "Name": "Blake Lizotte",
    "JerseyNumber": 46,
    "TeamId": 26,
    "Id": 710
  },
  {
    "Name": "Tobias Bjornfot",
    "JerseyNumber": 33,
    "TeamId": 26,
    "Id": 711
  },
  {
    "Name": "Matt Nieto",
    "JerseyNumber": 83,
    "TeamId": 28,
    "Id": 712
  },
  {
    "Name": "Patrick Marleau",
    "JerseyNumber": 12,
    "TeamId": 28,
    "Id": 713
  },
  {
    "Name": "Brent Burns",
    "JerseyNumber": 88,
    "TeamId": 28,
    "Id": 714
  },
  {
    "Name": "Marc-Edouard Vlasic",
    "JerseyNumber": 44,
    "TeamId": 28,
    "Id": 715
  },
  {
    "Name": "Logan Couture",
    "JerseyNumber": 39,
    "TeamId": 28,
    "Id": 716
  },
  {
    "Name": "Erik Karlsson",
    "JerseyNumber": 65,
    "TeamId": 28,
    "Id": 717
  },
  {
    "Name": "Greg Pateryn",
    "JerseyNumber": 2,
    "TeamId": 28,
    "Id": 718
  },
  {
    "Name": "Martin Jones",
    "JerseyNumber": 31,
    "TeamId": 28,
    "Id": 719
  },
  {
    "Name": "Evander Kane",
    "JerseyNumber": 9,
    "TeamId": 28,
    "Id": 720
  },
  {
    "Name": "Marcus Sorensen",
    "JerseyNumber": 20,
    "TeamId": 28,
    "Id": 721
  },
  {
    "Name": "Kurtis Gabriel",
    "JerseyNumber": 29,
    "TeamId": 28,
    "Id": 722
  },
  {
    "Name": "Tomas Hertl",
    "JerseyNumber": 48,
    "TeamId": 28,
    "Id": 723
  },
  {
    "Name": "Ryan Donato",
    "JerseyNumber": 16,
    "TeamId": 28,
    "Id": 724
  },
  {
    "Name": "Kevin Labanc",
    "JerseyNumber": 62,
    "TeamId": 28,
    "Id": 725
  },
  {
    "Name": "Timo Meier",
    "JerseyNumber": 28,
    "TeamId": 28,
    "Id": 726
  },
  {
    "Name": "Christian Jaros",
    "JerseyNumber": 47,
    "TeamId": 28,
    "Id": 727
  },
  {
    "Name": "Rudolfs Balcers",
    "JerseyNumber": 92,
    "TeamId": 28,
    "Id": 728
  },
  {
    "Name": "Noah Gregor",
    "JerseyNumber": 73,
    "TeamId": 28,
    "Id": 729
  },
  {
    "Name": "Dylan Gambrell",
    "JerseyNumber": 7,
    "TeamId": 28,
    "Id": 730
  },
  {
    "Name": "Jeffrey Viel",
    "JerseyNumber": 63,
    "TeamId": 28,
    "Id": 731
  },
  {
    "Name": "Mario Ferraro",
    "JerseyNumber": 38,
    "TeamId": 28,
    "Id": 732
  },
  {
    "Name": "Radim Simek",
    "JerseyNumber": 51,
    "TeamId": 28,
    "Id": 733
  },
  {
    "Name": "Josef Korenar",
    "JerseyNumber": 32,
    "TeamId": 28,
    "Id": 734
  },
  {
    "Name": "John Leonard",
    "JerseyNumber": 43,
    "TeamId": 28,
    "Id": 735
  },
  {
    "Name": "Nikolai Knyzhov",
    "JerseyNumber": 71,
    "TeamId": 28,
    "Id": 736
  },
  {
    "Name": "Fredrik Handemark",
    "JerseyNumber": 37,
    "TeamId": 28,
    "Id": 737
  },
  {
    "Name": "Boone Jenner",
    "JerseyNumber": 38,
    "TeamId": 29,
    "Id": 738
  },
  {
    "Name": "Zach Werenski",
    "JerseyNumber": 8,
    "TeamId": 29,
    "Id": 739
  },
  {
    "Name": "Emil Bemstrom",
    "JerseyNumber": 52,
    "TeamId": 29,
    "Id": 740
  },
  {
    "Name": "Gustav Nyquist",
    "JerseyNumber": 14,
    "TeamId": 29,
    "Id": 741
  },
  {
    "Name": "Michael Del Zotto",
    "JerseyNumber": 15,
    "TeamId": 29,
    "Id": 742
  },
  {
    "Name": "Zac Dalpe",
    "JerseyNumber": 26,
    "TeamId": 29,
    "Id": 743
  },
  {
    "Name": "Cam Atkinson",
    "JerseyNumber": 13,
    "TeamId": 29,
    "Id": 744
  },
  {
    "Name": "Scott Harrington",
    "JerseyNumber": 4,
    "TeamId": 29,
    "Id": 745
  },
  {
    "Name": "Stefan Matteau",
    "JerseyNumber": 23,
    "TeamId": 29,
    "Id": 746
  },
  {
    "Name": "Joonas Korpisalo",
    "JerseyNumber": 70,
    "TeamId": 29,
    "Id": 747
  },
  {
    "Name": "Oliver Bjorkstrand",
    "JerseyNumber": 28,
    "TeamId": 29,
    "Id": 748
  },
  {
    "Name": "Seth Jones",
    "JerseyNumber": 3,
    "TeamId": 29,
    "Id": 749
  },
  {
    "Name": "Max Domi",
    "JerseyNumber": 16,
    "TeamId": 29,
    "Id": 750
  },
  {
    "Name": "Ryan MacInnis",
    "JerseyNumber": 49,
    "TeamId": 29,
    "Id": 751
  },
  {
    "Name": "Elvis Merzlikins",
    "JerseyNumber": 90,
    "TeamId": 29,
    "Id": 752
  },
  {
    "Name": "Jack Roslovic",
    "JerseyNumber": 96,
    "TeamId": 29,
    "Id": 753
  },
  {
    "Name": "Gabriel Carlsson",
    "JerseyNumber": 53,
    "TeamId": 29,
    "Id": 754
  },
  {
    "Name": "Dean Kukan",
    "JerseyNumber": 46,
    "TeamId": 29,
    "Id": 755
  },
  {
    "Name": "Kevin Stenlund",
    "JerseyNumber": 11,
    "TeamId": 29,
    "Id": 756
  },
  {
    "Name": "Vladislav Gavrikov",
    "JerseyNumber": 44,
    "TeamId": 29,
    "Id": 757
  },
  {
    "Name": "Kole Sherwood",
    "JerseyNumber": 88,
    "TeamId": 29,
    "Id": 758
  },
  {
    "Name": "Patrik Laine",
    "JerseyNumber": 29,
    "TeamId": 29,
    "Id": 759
  },
  {
    "Name": "Andrew Peeke",
    "JerseyNumber": 2,
    "TeamId": 29,
    "Id": 760
  },
  {
    "Name": "Alexandre Texier",
    "JerseyNumber": 42,
    "TeamId": 29,
    "Id": 761
  },
  {
    "Name": "Eric Robinson",
    "JerseyNumber": 50,
    "TeamId": 29,
    "Id": 762
  },
  {
    "Name": "Mikko Lehtonen",
    "JerseyNumber": 43,
    "TeamId": 29,
    "Id": 763
  },
  {
    "Name": "Joshua Dunne",
    "JerseyNumber": 21,
    "TeamId": 29,
    "Id": 764
  },
  {
    "Name": "Joseph Cramarossa",
    "JerseyNumber": 56,
    "TeamId": 30,
    "Id": 765
  },
  {
    "Name": "Kyle Rau",
    "JerseyNumber": 37,
    "TeamId": 30,
    "Id": 766
  },
  {
    "Name": "Andrew Hammond",
    "JerseyNumber": 35,
    "TeamId": 30,
    "Id": 767
  },
  {
    "Name": "Luke Johnson",
    "JerseyNumber": 41,
    "TeamId": 30,
    "Id": 768
  },
  {
    "Name": "Dakota Mermis",
    "JerseyNumber": 57,
    "TeamId": 30,
    "Id": 769
  },
  {
    "Name": "Ryan Suter",
    "JerseyNumber": 20,
    "TeamId": 30,
    "Id": 770
  },
  {
    "Name": "Zach Parise",
    "JerseyNumber": 11,
    "TeamId": 30,
    "Id": 771
  },
  {
    "Name": "Nick Bonino",
    "JerseyNumber": 13,
    "TeamId": 30,
    "Id": 772
  },
  {
    "Name": "Ian Cole",
    "JerseyNumber": 28,
    "TeamId": 30,
    "Id": 773
  },
  {
    "Name": "Jared Spurgeon",
    "JerseyNumber": 46,
    "TeamId": 30,
    "Id": 774
  },
  {
    "Name": "Marcus Johansson",
    "JerseyNumber": 90,
    "TeamId": 30,
    "Id": 775
  },
  {
    "Name": "Marcus Foligno",
    "JerseyNumber": 17,
    "TeamId": 30,
    "Id": 776
  },
  {
    "Name": "Cam Talbot",
    "JerseyNumber": 33,
    "TeamId": 30,
    "Id": 777
  },
  {
    "Name": "Mats Zuccarello",
    "JerseyNumber": 36,
    "TeamId": 30,
    "Id": 778
  },
  {
    "Name": "Nick Bjugstad",
    "JerseyNumber": 27,
    "TeamId": 30,
    "Id": 779
  },
  {
    "Name": "Victor Rask",
    "JerseyNumber": 49,
    "TeamId": 30,
    "Id": 780
  },
  {
    "Name": "Jonas Brodin",
    "JerseyNumber": 25,
    "TeamId": 30,
    "Id": 781
  },
  {
    "Name": "Brad Hunt",
    "JerseyNumber": 77,
    "TeamId": 30,
    "Id": 782
  },
  {
    "Name": "Matt Dumba",
    "JerseyNumber": 24,
    "TeamId": 30,
    "Id": 783
  },
  {
    "Name": "Carson Soucy",
    "JerseyNumber": 21,
    "TeamId": 30,
    "Id": 784
  },
  {
    "Name": "Ryan Hartman",
    "JerseyNumber": 38,
    "TeamId": 30,
    "Id": 785
  },
  {
    "Name": "Kevin Fiala",
    "JerseyNumber": 22,
    "TeamId": 30,
    "Id": 786
  },
  {
    "Name": "Kaapo Kahkonen",
    "JerseyNumber": 34,
    "TeamId": 30,
    "Id": 787
  },
  {
    "Name": "Jordan Greenway",
    "JerseyNumber": 18,
    "TeamId": 30,
    "Id": 788
  },
  {
    "Name": "Joel Eriksson Ek",
    "JerseyNumber": 14,
    "TeamId": 30,
    "Id": 789
  },
  {
    "Name": "Kirill Kaprizov",
    "JerseyNumber": 97,
    "TeamId": 30,
    "Id": 790
  },
  {
    "Name": "Nico Sturm",
    "JerseyNumber": 7,
    "TeamId": 30,
    "Id": 791
  },
  {
    "Name": "Bryan Little",
    "JerseyNumber": 18,
    "TeamId": 52,
    "Id": 792
  },
  {
    "Name": "Nathan Beaulieu",
    "JerseyNumber": 88,
    "TeamId": 52,
    "Id": 793
  },
  {
    "Name": "Nate Thompson",
    "JerseyNumber": 11,
    "TeamId": 52,
    "Id": 794
  },
  {
    "Name": "Blake Wheeler",
    "JerseyNumber": 26,
    "TeamId": 52,
    "Id": 795
  },
  {
    "Name": "Paul Stastny",
    "JerseyNumber": 25,
    "TeamId": 52,
    "Id": 796
  },
  {
    "Name": "Trevor Lewis",
    "JerseyNumber": 23,
    "TeamId": 52,
    "Id": 797
  },
  {
    "Name": "Mathieu Perreault",
    "JerseyNumber": 85,
    "TeamId": 52,
    "Id": 798
  },
  {
    "Name": "Jordie Benn",
    "JerseyNumber": 40,
    "TeamId": 52,
    "Id": 799
  },
  {
    "Name": "Derek Forbort",
    "JerseyNumber": 24,
    "TeamId": 52,
    "Id": 800
  },
  {
    "Name": "Laurent Brossoit",
    "JerseyNumber": 30,
    "TeamId": 52,
    "Id": 801
  },
  {
    "Name": "Dylan DeMelo",
    "JerseyNumber": 2,
    "TeamId": 52,
    "Id": 802
  },
  {
    "Name": "Adam Lowry",
    "JerseyNumber": 17,
    "TeamId": 52,
    "Id": 803
  },
  {
    "Name": "Mark Scheifele",
    "JerseyNumber": 55,
    "TeamId": 52,
    "Id": 804
  },
  {
    "Name": "Connor Hellebuyck",
    "JerseyNumber": 37,
    "TeamId": 52,
    "Id": 805
  },
  {
    "Name": "Tucker Poolman",
    "JerseyNumber": 3,
    "TeamId": 52,
    "Id": 806
  },
  {
    "Name": "Andrew Copp",
    "JerseyNumber": 9,
    "TeamId": 52,
    "Id": 807
  },
  {
    "Name": "Josh Morrissey",
    "JerseyNumber": 44,
    "TeamId": 52,
    "Id": 808
  },
  {
    "Name": "Nikolaj Ehlers",
    "JerseyNumber": 27,
    "TeamId": 52,
    "Id": 809
  },
  {
    "Name": "Kyle Connor",
    "JerseyNumber": 81,
    "TeamId": 52,
    "Id": 810
  },
  {
    "Name": "Jansen Harkins",
    "JerseyNumber": 12,
    "TeamId": 52,
    "Id": 811
  },
  {
    "Name": "Mason Appleton",
    "JerseyNumber": 22,
    "TeamId": 52,
    "Id": 812
  },
  {
    "Name": "Sami Niku",
    "JerseyNumber": 8,
    "TeamId": 52,
    "Id": 813
  },
  {
    "Name": "Logan Stanley",
    "JerseyNumber": 64,
    "TeamId": 52,
    "Id": 814
  },
  {
    "Name": "Pierre-Luc Dubois",
    "JerseyNumber": 13,
    "TeamId": 52,
    "Id": 815
  },
  {
    "Name": "Neal Pionk",
    "JerseyNumber": 4,
    "TeamId": 52,
    "Id": 816
  },
  {
    "Name": "Darcy Kuemper",
    "JerseyNumber": 35,
    "TeamId": 53,
    "Id": 817
  },
  {
    "Name": "Jordan Gross",
    "JerseyNumber": 79,
    "TeamId": 53,
    "Id": 818
  },
  {
    "Name": "Alex Goligoski",
    "JerseyNumber": 33,
    "TeamId": 53,
    "Id": 819
  },
  {
    "Name": "Niklas Hjalmarsson",
    "JerseyNumber": 4,
    "TeamId": 53,
    "Id": 820
  },
  {
    "Name": "Derick Brassard",
    "JerseyNumber": 16,
    "TeamId": 53,
    "Id": 821
  },
  {
    "Name": "Phil Kessel",
    "JerseyNumber": 81,
    "TeamId": 53,
    "Id": 822
  },
  {
    "Name": "Jason Demers",
    "JerseyNumber": 55,
    "TeamId": 53,
    "Id": 823
  },
  {
    "Name": "Oliver Ekman-Larsson",
    "JerseyNumber": 23,
    "TeamId": 53,
    "Id": 824
  },
  {
    "Name": "Johan Larsson",
    "JerseyNumber": 22,
    "TeamId": 53,
    "Id": 825
  },
  {
    "Name": "Tyler Pitlick",
    "JerseyNumber": 17,
    "TeamId": 53,
    "Id": 826
  },
  {
    "Name": "Antti Raanta",
    "JerseyNumber": 32,
    "TeamId": 53,
    "Id": 827
  },
  {
    "Name": "John Hayden",
    "JerseyNumber": 15,
    "TeamId": 53,
    "Id": 828
  },
  {
    "Name": "Jordan Oesterle",
    "JerseyNumber": 82,
    "TeamId": 53,
    "Id": 829
  },
  {
    "Name": "Nick Schmaltz",
    "JerseyNumber": 8,
    "TeamId": 53,
    "Id": 830
  },
  {
    "Name": "Christian Dvorak",
    "JerseyNumber": 18,
    "TeamId": 53,
    "Id": 831
  },
  {
    "Name": "Michael Bunting",
    "JerseyNumber": 58,
    "TeamId": 53,
    "Id": 832
  },
  {
    "Name": "Dryden Hunt",
    "JerseyNumber": 28,
    "TeamId": 53,
    "Id": 833
  },
  {
    "Name": "Christian Fischer",
    "JerseyNumber": 36,
    "TeamId": 53,
    "Id": 834
  },
  {
    "Name": "Lawson Crouse",
    "JerseyNumber": 67,
    "TeamId": 53,
    "Id": 835
  },
  {
    "Name": "Adin Hill",
    "JerseyNumber": 31,
    "TeamId": 53,
    "Id": 836
  },
  {
    "Name": "Conor Garland",
    "JerseyNumber": 83,
    "TeamId": 53,
    "Id": 837
  },
  {
    "Name": "Clayton Keller",
    "JerseyNumber": 9,
    "TeamId": 53,
    "Id": 838
  },
  {
    "Name": "Jakob Chychrun",
    "JerseyNumber": 6,
    "TeamId": 53,
    "Id": 839
  },
  {
    "Name": "Ilya Lyubushkin",
    "JerseyNumber": 46,
    "TeamId": 53,
    "Id": 840
  },
  {
    "Name": "Ryan Reaves",
    "JerseyNumber": 75,
    "TeamId": 54,
    "Id": 841
  },
  {
    "Name": "Nick Holden",
    "JerseyNumber": 22,
    "TeamId": 54,
    "Id": 842
  },
  {
    "Name": "Marc-Andre Fleury",
    "JerseyNumber": 29,
    "TeamId": 54,
    "Id": 843
  },
  {
    "Name": "Max Pacioretty",
    "JerseyNumber": 67,
    "TeamId": 54,
    "Id": 844
  },
  {
    "Name": "Alec Martinez",
    "JerseyNumber": 23,
    "TeamId": 54,
    "Id": 845
  },
  {
    "Name": "Alex Pietrangelo",
    "JerseyNumber": 7,
    "TeamId": 54,
    "Id": 846
  },
  {
    "Name": "Brayden McNabb",
    "JerseyNumber": 3,
    "TeamId": 54,
    "Id": 847
  },
  {
    "Name": "Reilly Smith",
    "JerseyNumber": 19,
    "TeamId": 54,
    "Id": 848
  },
  {
    "Name": "Robin Lehner",
    "JerseyNumber": 90,
    "TeamId": 54,
    "Id": 849
  },
  {
    "Name": "Mark Stone",
    "JerseyNumber": 61,
    "TeamId": 54,
    "Id": 850
  },
  {
    "Name": "Tomas Jurco",
    "JerseyNumber": 13,
    "TeamId": 54,
    "Id": 851
  },
  {
    "Name": "William Karlsson",
    "JerseyNumber": 71,
    "TeamId": 54,
    "Id": 852
  },
  {
    "Name": "Jonathan Marchessault",
    "JerseyNumber": 81,
    "TeamId": 54,
    "Id": 853
  },
  {
    "Name": "Chandler Stephenson",
    "JerseyNumber": 20,
    "TeamId": 54,
    "Id": 854
  },
  {
    "Name": "Mattias Janmark",
    "JerseyNumber": 26,
    "TeamId": 54,
    "Id": 855
  },
  {
    "Name": "Shea Theodore",
    "JerseyNumber": 27,
    "TeamId": 54,
    "Id": 856
  },
  {
    "Name": "William Carrier",
    "JerseyNumber": 28,
    "TeamId": 54,
    "Id": 857
  },
  {
    "Name": "Tomas Nosek",
    "JerseyNumber": 92,
    "TeamId": 54,
    "Id": 858
  },
  {
    "Name": "Alex Tuch",
    "JerseyNumber": 89,
    "TeamId": 54,
    "Id": 859
  },
  {
    "Name": "Keegan Kolesar",
    "JerseyNumber": 55,
    "TeamId": 54,
    "Id": 860
  },
  {
    "Name": "Nicolas Roy",
    "JerseyNumber": 10,
    "TeamId": 54,
    "Id": 861
  },
  {
    "Name": "Dylan Coghlan",
    "JerseyNumber": 52,
    "TeamId": 54,
    "Id": 862
  },
  {
    "Name": "Nicolas Hague",
    "JerseyNumber": 14,
    "TeamId": 54,
    "Id": 863
  },
  {
    "Name": "Zach Whitecloud",
    "JerseyNumber": 2,
    "TeamId": 54,
    "Id": 864
  }
]')
    with (Name nvarchar(max), JerseyNumber int, TeamId int, Id int)
set identity_insert Players off
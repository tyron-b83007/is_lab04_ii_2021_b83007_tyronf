using CSharpFunctionalExtensions;

namespace Lab2.Domain.Core.Entities
{
    public abstract class Entity : Entity<long>
    {
        protected Entity(long id) : base(id) { }
    }
}
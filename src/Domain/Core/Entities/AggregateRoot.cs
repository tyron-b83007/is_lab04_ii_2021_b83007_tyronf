namespace Lab2.Domain.Core.Entities
{
    public abstract class AggregateRoot : Entity
    {
        protected AggregateRoot(long id) : base(id) { }
    }
}
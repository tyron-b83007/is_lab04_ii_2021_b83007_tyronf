using Lab2.Domain.Core.ValueObjects;

namespace Lab2.Domain.HockeyLeague.DTOs
{
    public class TeamDTO
    {
        public long Id { get; }
        public RequiredString Name { get; }

        public TeamDTO(long id, RequiredString name)
        {
            Id = id;
            Name = name;
        }
    }
}

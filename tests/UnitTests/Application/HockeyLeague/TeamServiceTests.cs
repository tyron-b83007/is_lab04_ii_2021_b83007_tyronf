﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Lab2.Application.HockeyLeague.Implementations;
using Lab2.Domain.Core.Helpers;
using Lab2.Domain.Core.ValueObjects;
using Lab2.Domain.HockeyLeague.DTOs;
using Lab2.Domain.HockeyLeague.Entities;
using Lab2.Domain.HockeyLeague.Repositories;
using Lab2.Domain.HockeyLeague.ValueObjects;
using Moq;
using Xunit;

namespace Lab2.UnitTests.Application.HockeyLeague
{
    public class TeamServiceTests
    {
        //Global arrange
        private static readonly RequiredString TeamName = RequiredString.TryCreate("Team name").Success();
        private static IEnumerable<TeamDTO> GetTeams()
        {
            const int teamCount = 1000;
            for (int i = 0; i < teamCount; ++i)
            {
                yield return new TeamDTO(i, TeamName);
            }
        }

        [Fact]
        public async Task GetTeamsAsyncShouldReturnTeamDTOs()
        {
            // arrange
            var teams = GetTeams().ToList();
            var mockTeamRepository = new Mock<ITeamRepository>();

            //Definimos el comportamiento del método GetAllAsync y el resultado que dará
            mockTeamRepository.Setup(repo => repo.GetAllAsync()).ReturnsAsync(teams);
            //Injectamos el repositorio
            var teamService = new TeamService(mockTeamRepository.Object);

            // act
            var results = await teamService.GetTeamsAsync();

            // assert
            results.Should().BeEquivalentTo(teams);
        }

        [Fact]
        public async Task GetTeamByIdAsyncWithValidIdShouldReturnTeam()
        {
            // arrange
            const int id = 1;
            var team = new Team(TeamName, id: id);
            var mockTeamRepository = new Mock<ITeamRepository>();

            //Definimos el comportamiento del método GetByIdAsyncy el resultado que dará
            mockTeamRepository.Setup(repo => repo.GetByIdAsync(id)).ReturnsAsync(team);
            //Injectamos el repositorio
            var teamService = new TeamService(mockTeamRepository.Object);

            // act
            var result = await teamService.GetTeamByIdAsync(id);

            // assert
            result.Should().Be(team);
        }

        [Fact]
        public async Task AddPlayerToTeamRosterAsyncWithValidPlayerShouldAddPlayer()
        {
            // arrange
            const int id = 1;
            var team = new Team(TeamName, id: id);
            var player = new Player(
                RequiredString.TryCreate("Player name").Success(), null,
                JerseyNumber.TryCreate(1).Success(),
                id
            );

            var mockTeamRepository = new Mock<ITeamRepository>();
            var teamService = new TeamService(mockTeamRepository.Object);

            // act
            await teamService.AddPlayerToTeamRosterAsync(team, player);

            // assert
            team.Roster.Should().Contain(player);
            player.Team.Should().Be(team);
            //Verify, verifica que el metodo SaveAsync(team) haya sido llamado una vez
            mockTeamRepository.Verify(repo => repo.SaveAsync(team), Times.Once);
        }

        [Fact]
        public async Task RemovePlayerToTeamRosterAsyncShouldRemovePlayer()
        {
            // arrange
            const int id = 1;
            var team = new Team(TeamName, id: id);
            var player = new Player(
                RequiredString.TryCreate("Player name").Success(), null,
                JerseyNumber.TryCreate(1).Success(),
                id
            );
            var mockTeamRepository = new Mock<ITeamRepository>();
            var teamService = new TeamService(mockTeamRepository.Object);
            await teamService.AddPlayerToTeamRosterAsync(team, player);

            // act
            await teamService.DeletePlayerAndRemoveFromTeamRosterAsync(team,player);

            // assert
            team.Roster.Should().NotContain(player);
            player.Team.Should().BeNull();
            //Verifica que el metodo DeletePlayer(player) haya sido llamado una vez
            mockTeamRepository.Verify(repo => repo.DeletePlayer(player), Times.Once);
            //Verifica que el metodo SaveAsync(team) haya sido llamado exactamente 2 veces
            mockTeamRepository.Verify(repo => repo.SaveAsync(team), Times.Exactly(2));
        }
    }
}
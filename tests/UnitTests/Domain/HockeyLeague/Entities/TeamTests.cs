﻿using FluentAssertions;
using Lab2.Domain.Core.Exceptions;
using Lab2.Domain.Core.Helpers;
using Lab2.Domain.Core.ValueObjects;
using Lab2.Domain.HockeyLeague.Entities;
using Lab2.Domain.HockeyLeague.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Lab2.UnitTests.Domain.HockeyLeague.Entities
{
    public class TeamTests
    {
        //Global arrange
        private static readonly RequiredString TeamName = RequiredString.TryCreate("Team name").Success();
        private static readonly RequiredString PlayerName = RequiredString.TryCreate("Player name").Success();


        private IEnumerable<Player> GetRoster()
        {
            for (var i = 1; i <= Team.MaxRosterSize + 1; ++i)
            {
                var jerseyNumber = JerseyNumber.TryCreate(i).Success();
                yield return new Player(PlayerName, new Team(TeamName), jerseyNumber, id: i);
            }
        }

        [Fact]
        public void NewTeamHasAnEmptyRoster()
        {
            //arrange

            // act
            var team = new Team(TeamName);

            // assert
            team.Roster.Should().BeEmpty();
        }

        [Fact]
        public void AddOnePlayerShouldNotThrow()
        {
            // arrange
            var team = new Team(TeamName);

            //  act
            var player = GetRoster().First();

            // assert
            team.Invoking(t => t.AddPlayerToRoster(player)).Should().NotThrow();
        }


        [Fact]
        public void AddSamePlayerToRosterTwiceShouldThrow()
        {
            // arrange
            var team = new Team(TeamName);

            //  act
            var player = GetRoster().First();
            team.AddPlayerToRoster(player);

            // assert
            team.Invoking(t => t.AddPlayerToRoster(player))
                .Should().Throw<DomainException>()
                .WithMessage("Player is already in the team.");
        }

        [Fact]
        public void AddDifferentPlayersWithSameJerseyNumberToRosterShouldThrow()
        {
            // arrange
            var team = new Team(TeamName);

            //  act
            var player = GetRoster().First();
            var playerWithRepeatedJersey = new Player(player.Name, player.Team, player.JerseyNumber, player.Id + 1);
            team.AddPlayerToRoster(player);

            // assert
            team.Invoking(t => t.AddPlayerToRoster(playerWithRepeatedJersey)).Should().Throw<DomainException>()
                .WithMessage("A player with the selected jersey number is already registered in the team.");
        }

        [Fact]
        public void AddPlayerToFullRosterShouldThrow()
        {
            // arrange
            var team = new Team(TeamName);
            var roster = GetRoster().ToList();

            //  act
            foreach (var player in roster.Take(Team.MaxRosterSize))
            {
                team.AddPlayerToRoster(player);
            }

            // assert
            team.Invoking(t => t.AddPlayerToRoster(roster.Last()))
                .Should().Throw<DomainException>()
                .WithMessage("Roster is at it's maximum capacity.");
        }

        [Fact]
        public void AddPlayerToRosterShouldAssignTeam()
        {
            // arrange
            var team = new Team(TeamName);
            var player = GetRoster().First();

            //  act
            team.AddPlayerToRoster(player);

            // assert
            player.Team.Should().Be(team);
        }

        [Fact]
        public void RemovePlayerFromRosterShouldRemoveFromTeamRoster()
        {
            // arrange
            var team = new Team(TeamName);
            var player = GetRoster().First();
            team.AddPlayerToRoster(player);

            // act
            team.RemovePlayerFromRoster(player);

            // arrange
            team.Roster.Should().BeEmpty();
        }

        [Fact]
        public void RemovePlayerFromRosterShouldUnassignTeamFromPlayer()
        {
            // arrange
            var team = new Team(TeamName);
            var player = GetRoster().First();
            team.AddPlayerToRoster(player);

            // act
            team.RemovePlayerFromRoster(player);

            // arrange
            player.Team.Should().BeNull();
        }
    }
}

﻿using FluentAssertions;
using Lab2.Domain.Core.Helpers;
using Lab2.Domain.Core.ValueObjects;
using Lab2.Domain.HockeyLeague.Entities;
using Lab2.Domain.HockeyLeague.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Lab2.UnitTests.Domain.HockeyLeague.Entities
{
    public class PlayerTests
    {
        private static readonly Team Team = new Team(RequiredString.TryCreate("Team name").Success());
        private static readonly RequiredString PlayerName = RequiredString.TryCreate("Player name").Success();
        private static readonly JerseyNumber JerseyNumber = JerseyNumber.TryCreate(50).Success();


        [Fact]
        public void AssignTeamSetsTeamCorrectly()
        {
            // arrange
            var player = new Player(PlayerName, null, JerseyNumber);

            // act
            player.AssignTeam(Team);

            // assert
            player.Team.Should().Be(Team);
        }

        [Fact]
        public void TwoPlayersWithSameIdAreEqual()
        {
            // arrange
            var player1 = new Player(PlayerName, null, JerseyNumber, id: 5);
            var player2 = new Player(PlayerName, Team, JerseyNumber, id: 5);

            //act

            // assert
            player1.Should().Be(player2);
        }

    }
}

﻿using FluentAssertions;
using Lab2.Domain.Core.Helpers;
using Lab2.Domain.HockeyLeague.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Lab2.UnitTests.Domain.HockeyLeague.ValueObjects
{
    public class JerseryNumberTests
    {
        [Fact]
        public void TryCreateNullNumberReturnsValidationError()
        {
            //arrange
            int? number = null;

            //act
            var result = JerseyNumber.TryCreate(number);

            //assert
            result.IsSuccess.Should().BeFalse();
            result.Fail().Should().Be(new JerseyNumber.NumberIsNull());
        }

        [Fact]
        public void TryCreateNegativeNumbersReturnsValidationError()
        {
            //arrange
            int? number = -1;

            //act
            var result = JerseyNumber.TryCreate(number);

            //assert
            result.IsSuccess.Should().BeFalse();
            result.Fail().Should().Be(new JerseyNumber.NumberTooSmall(JerseyNumber.MinNumber));
        }

        [Fact]
        public void TryCreateZeroReturnsValidationError()
        {
            //arrange
            int? number = 0;

            //act
            var result = JerseyNumber.TryCreate(number);

            //assert
            result.IsSuccess.Should().BeFalse();
            result.Fail().Should().Be(new JerseyNumber.NumberTooSmall(JerseyNumber.MinNumber));
        }

        [Fact]
        public void TryCreateValidateNumberReturnsJerseyNumber()
        {
            //arrange
            int? number = 50;

            //act
            var result = JerseyNumber.TryCreate(number);

            //assert
            result.IsSuccess.Should().BeTrue();
            result.Success().Number.Should().Be(number.Value);
        }

        [Fact]
        public void TryCreateBigNumberReturnsValidationError()
        {
            // arrange
            int? number = JerseyNumber.MaxNumber + 1;

            // act
            var result = JerseyNumber.TryCreate(number);

            // assert
            result.IsSuccess.Should().BeFalse();
            result.Fail().Should().Be(new JerseyNumber.NumberTooBig(JerseyNumber.MaxNumber));
        }

        [Fact]
        public void ToStringReturnsNumberAsString()
        {
            // arrange
            int? number = 50;
            var result = JerseyNumber.TryCreate(number);

            // act
            var toString = result.Success().ToString();

            // assert
            toString.Should().Be(number.ToString());
        }


        [Fact]
        public void TwoJerseysWithSameNumberShouldBeEqual()
        {
            // arrange
            int? number = 50;
            var result1 = JerseyNumber.TryCreate(number);
            var result2 = JerseyNumber.TryCreate(number);

            // act
            var jerseyNumber1 = result1.Success();
            var jerseyNumber2 = result2.Success();

            // assert
            jerseyNumber1.Should().Be(jerseyNumber2);
        }
    }
}

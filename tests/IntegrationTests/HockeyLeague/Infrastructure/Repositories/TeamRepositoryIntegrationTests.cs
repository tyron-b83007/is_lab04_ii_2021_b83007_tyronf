﻿using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Lab2.Domain.Core.Helpers;
using Lab2.Domain.Core.ValueObjects;
using Lab2.Domain.HockeyLeague.Entities;
using Lab2.Domain.HockeyLeague.Repositories;
using Lab2.Domain.HockeyLeague.ValueObjects;
using Lab2.Server;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
namespace Lab2.IntegrationTests.HockeyLeague.Infrastructure.Repositories
{
    public class TeamRepositoryIntegrationTests : IClassFixture<HockeyLeagueWebApplicationFactory<Startup>>
    {
        private readonly HockeyLeagueWebApplicationFactory<Startup> _factory;

        public TeamRepositoryIntegrationTests(HockeyLeagueWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task GetAllAsyncShouldReturnAllTeams()
        {
            const int teamCount = 31;
            // arrange
            var repository = _factory.Server.Services.GetRequiredService<ITeamRepository>();
            // act
            var teams = await repository.GetAllAsync();
            // assert
            teams.Should().HaveCount(teamCount);
        }

        [Fact]
        public async Task GetByIdForInvalidIdShouldReturnNull()
        {
            const int teamId = 300;
            // arrange
            var repository = _factory.Server.Services.GetRequiredService<ITeamRepository>();
            // act
            var team = await repository.GetByIdAsync(teamId);
            // assert
            team.Should().BeNull();
        }

        [Fact]
        public async Task GetByIdForValidIdShouldReturnTeamWithRelatedData()
        {
            // arrange
            const string teamName = "New York Rangers";
            const string playerName = "Artemi Panarin";
            const int teamId = 3;
            var repository = _factory.Server.Services.GetRequiredService<ITeamRepository>();
            // act
            var team = await repository.GetByIdAsync(teamId);
            // assert
            team.Should().NotBeNull();
            team!.Name.Value.Should().Be(teamName);
            team!.Roster.Should().Contain(p => p.Name.Value == playerName);
        }

        [Fact]
        public async Task DeletePlayerShouldDeletePlayerFromDb()
        {
            // arrange
            const int teamId = 3;
            const int playerId = 67;
            var repository =
            _factory.Server.Services.GetRequiredService<ITeamRepository>();
            var team = await repository.GetByIdAsync(teamId);
            // act
            await repository.DeletePlayer(team!.Roster.First(p => p.Id == playerId));
            // assert
            team.Roster.Should().NotContain(p => p.Id == playerId);
            // cleanup
            _factory.SeedDatabaseForTests((DbContext)repository.UnitOfWork);
        }

        [Fact]
        public async Task SaveAsyncShouldPersistNewPlayer()
        {
            // arrange
            const int teamId = 3;
            const int playerJersey = 50;
            var repository =
            _factory.Server.Services.GetRequiredService<ITeamRepository>();
            var team = await repository.GetByIdAsync(teamId);
            var player = new Player(
            RequiredString.TryCreate("Player name").Success(), null, JerseyNumber.TryCreate(playerJersey).Success());
            team!.AddPlayerToRoster(player);
            var playerCount = team.Roster.Count;
            // act
            await repository.SaveAsync(team);
            team = await repository.GetByIdAsync(teamId);
            // assert
            team!.Roster.Should().HaveCount(playerCount);
            // cleanup
            _factory.SeedDatabaseForTests((DbContext)repository.UnitOfWork);
        }
    }
}